import React from 'react'
import {TinyBookCard} from '../containers/Book'

export default ({bookList}) => (
    <div>
        { bookList.map((isbn) => (
            <TinyBookCard key={isbn} isbn={isbn} />
        ))}
    </div>
)