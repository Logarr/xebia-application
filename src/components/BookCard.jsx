import React from 'react'

import * as s from '../styled/BookCard.style'
import { StyledCartActionButton }from '../styled/Buttons.style'

export default ({isbn, title, price, cover, qty, addToCart, removeFromCart, openBook}) => (
    <s.Container onClick={() => {
        openBook(isbn)
    }}>
        <s.Header title={title}>
            {title}
        </s.Header>
        <div>
            <s.CoverContainer>
                <img src={cover} alt={title}/>
            </s.CoverContainer>
            <s.Details>
                <s.CartActions>
                    <s.CartButtons>
                        <StyledCartActionButton
                            onClick={(e) => {
                                e.stopPropagation();
                                addToCart(isbn);
                            }}
                        >
                            +
                        </StyledCartActionButton>
                        {qty > 0 &&
                        <StyledCartActionButton
                            onClick={(e) => {
                                e.stopPropagation();
                                removeFromCart(isbn);
                            }}
                        >
                            -
                        </StyledCartActionButton>
                        }
                    </s.CartButtons>
                    <s.Qty>{qty > 0 ? qty : ''}</s.Qty>
                </s.CartActions>
                <s.Price>{price}$</s.Price>
            </s.Details>
        </div>
    </s.Container>
)