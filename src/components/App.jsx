import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import BookList from '../containers/BookList'
import Cart from '../components/Cart'
import {CartSummary} from '../containers/Cart'
import * as s from '../styled/App.style'
import {BookModal} from "../containers/Book";

export default ({openBook}) => (
    <Router>
        <div>
            <s.Header>
                <s.Title>
                    <Link to="/" title="Books List">Henri Potier's Bookshop</Link>
                </s.Title>
                <CartSummary/>
            </s.Header>
            <Route exact path="/" component={BookList} />
            <Route exact path="/cart" component={Cart} />
            {openBook &&
                <BookModal isbn={openBook}/>
            }
        </div>
    </Router>
)