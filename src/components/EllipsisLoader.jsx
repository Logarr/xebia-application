import React from 'react'
import styled, { keyframes } from 'styled-components'

const grow = keyframes`
  0% {
    transform: scale(0);
  }
  100% {
    transform: scale(1);
  }
`;

const shrink = keyframes`
  0% {
    transform: scale(1);
  }
  100% {
    transform: scale(0);
  }
`;

const moveRight = keyframes`
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(11px, 0);
  }
`;

const Ellipsis = styled.div`
  display: inline-block;
  position: relative;
  width: 40px;
  height: 20px;
  margin: auto auto;
`;

const Child = styled.div`
  position: absolute;
  top: 7px;
  width: 6px;
  height: 6px;
  border-radius: 50%;
  background: white;
  animation-timing-function: cubic-bezier(0, 1, 1, 0);
  left: ${props => props.left};
  animation: ${props => props.animation} 0.6s infinite;
`;

export default () => {
    return (
        <Ellipsis>
            <Child left='2px' animation={ grow }/>
            <Child left='2px' animation={ moveRight }/>
            <Child left='11px' animation={ moveRight }/>
            <Child left='11px' animation={ shrink }/>
        </Ellipsis>
    );
};