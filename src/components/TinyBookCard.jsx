import React from 'react'

import * as s from '../styled/TinyBookCard.style'
import {StyledCartActionButton} from "../styled/Buttons.style";

export default ({isbn, title, price, cover, synopsis, qty, addToCart, removeFromCart, openBook}) => {
    const wholeSynopsis = synopsis.join(' ');
    return (
        <s.TinyBookCardContainer onClick={() => openBook(isbn)}>
            <s.Title>
                {title}
            </s.Title>
            <s.SummaryContainer>
                <s.DetailsContainer>
                    <s.Cover>
                        <img src={cover} alt={title}/>
                    </s.Cover>
                    <s.CartContainer>
                        <s.CartActions>
                            <StyledCartActionButton
                                onClick={(e) => {
                                    e.stopPropagation();
                                    addToCart(isbn);
                                }}
                            >
                                +
                            </StyledCartActionButton>
                            <StyledCartActionButton
                                onClick={(e) => {
                                    e.stopPropagation();
                                    removeFromCart(isbn);
                                }}
                            >
                                -
                            </StyledCartActionButton>
                            <s.Qty>
                                {qty}
                            </s.Qty>
                        </s.CartActions>
                        <s.Price>
                            {price * qty}$
                        </s.Price>
                    </s.CartContainer>
                </s.DetailsContainer>
                <s.Synopsis>
                    { wholeSynopsis.slice(0, 600)}{wholeSynopsis.length > 600 && '...' }
                </s.Synopsis>
            </s.SummaryContainer>
        </s.TinyBookCardContainer>
    );
}