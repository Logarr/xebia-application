import React from 'react'

import * as s from '../styled/BookList.style'

export default ({filter, sort, setTextFilter, setMaxPriceFilter, toggleTitleSort, togglePriceSort}) => (
    <s.HeaderContainer>
        <s.FilterContainer>
            <s.TextSearch
                type="text"
                value={filter.text}
                placeholder="Search.."
                onChange={(e) => setTextFilter(e.target.value)}
            />
        </s.FilterContainer>
        <s.SortContainer>
            <div>
                <label>Sort by: </label>
                <a
                    className={sort.key === 'title' ? sort.order : ''}
                    onClick={toggleTitleSort}
                >
                    Title
                </a>
                <a
                    className={sort.key === 'price' ? sort.order : ''}
                    onClick={togglePriceSort}
                >
                    Price
                </a>
            </div>
            <div>
                <label>Max. Price</label>
                <input
                    type="number"
                    min="0"
                    value={filter.maxPrice === Infinity ? '' : filter.maxPrice}
                    onChange={(e) => setMaxPriceFilter(e.target.value)}
                />
            </div>
        </s.SortContainer>
    </s.HeaderContainer>
);

