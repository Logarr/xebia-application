import React from 'react'

import {BuyButton} from '../containers/Cart'
import BooksSelection from '../containers/BooksSelection'

import * as s from '../styled/Cart.style'

export default () => (
    <s.CartContainer>
        <s.Header>
            <h1>Your cart</h1>
            <BuyButton />
        </s.Header>
        <div>
            <BooksSelection />
        </div>
    </s.CartContainer>
);