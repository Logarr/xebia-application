import React from 'react'
import {Cart} from "../../styled/App.style";

import styled from 'styled-components'

const CheckIcon = styled.img`
  height: 10px;
  margin-left: 5px;
  margin-right: 10px;
`;

export default ({cartPrice, history}) => (
    <Cart onClick={() => history.push('/cart')}>
        <div>
            <span>{cartPrice}$</span>{cartPrice > 0 && <CheckIcon title="Best price" src="/assets/img/check.png"/>}
        </div>
        <img src="/assets/img/cart.png"/>
    </Cart>
)