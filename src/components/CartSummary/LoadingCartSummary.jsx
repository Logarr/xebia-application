import React from 'react'

import {Cart} from "../../styled/App.style";
import EllipsisLoader from '../EllipsisLoader'

export default () => (
    <Cart>
        <EllipsisLoader/>
    </Cart>
)