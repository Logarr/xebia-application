import React from 'react'

import * as s from '../../styled/CartSummary.style'
import {Cart} from "../../styled/App.style";

export default ({bookList, booksPrice, fetchOffers, history}) => (
    <Cart onClick={() => history.push('/cart')}>
        <s.PendingCartSummary>
            <span>{booksPrice}$</span>
            {bookList.length > 0 &&
                <a
                    onClick={(e) => {
                        e.stopPropagation();
                        fetchOffers(bookList)
                    }}
                >
                    Check for magical Offers !
                </a>
            }
        </s.PendingCartSummary>
        <img src="/assets/img/cart.png"/>
    </Cart>
)