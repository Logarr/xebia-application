import React from 'react';
import * as s from '../styled/BookModal.style'
import {StyledCartActionButton} from "../styled/Buttons.style";

export default ({isbn, title, price, cover, synopsis, qty, addToCart, removeFromCart, closeBook}) => (
    <s.ModalContainer onClick={() => closeBook()}>
        <s.ModalContent onClick={(e) => e.stopPropagation()}>
            <s.Header>
                <s.Title>{title}</s.Title>
                <s.CloseButton onClick={() => closeBook()}>X</s.CloseButton>
            </s.Header>
            <s.TopPanel>
                <s.Cover>
                    <img src={cover} alt={title} />
                </s.Cover>
                <s.Cart>
                    <s.Price>
                        {price}$
                    </s.Price>
                    <s.CartActions>
                        <s.Buttons>
                            <StyledCartActionButton onClick={() => addToCart(isbn)}>+</StyledCartActionButton>
                            <StyledCartActionButton onClick={() => removeFromCart(isbn)}>-</StyledCartActionButton>
                        </s.Buttons>
                        <span>
                            {qty}
                        </span>
                    </s.CartActions>
                </s.Cart>
            </s.TopPanel>
            <s.Synopsis>
                {synopsis.join(' ')}
            </s.Synopsis>
        </s.ModalContent>
    </s.ModalContainer>
);

