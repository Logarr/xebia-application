import React from 'react'
import {StyledCartActionButton} from "../../styled/Buttons.style";
import * as s from '../../styled/BuyButton.style'

export default ({bookList, booksPrice}) => (
    <div>
        { bookList.length > 0 &&
            <s.BuyButton
                as={StyledCartActionButton}
                onClick={/*istanbul ignore next*/() => alert('thanks for your purchase, we will get back to you shortly.\n\n -Henri Potier\'s sales team')}
            >
                <s.DiscountPrice className="booksPrice">{booksPrice}$</s.DiscountPrice>
                <span>We were unable to get the best offer for your selection, but don't worry, you can still pay the full price ;)</span>
            </s.BuyButton>
        }
    </div>
);