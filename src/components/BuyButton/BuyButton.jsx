import React from 'react'
import {StyledCartActionButton} from "../../styled/Buttons.style";
import * as s from '../../styled/BuyButton.style'

export default ({bookList, cartPrice, booksPrice}) => (
    <div>
        { bookList.length > 0 &&
            <s.BuyButton
                as={StyledCartActionButton}
                onClick={/*istanbul ignore next*/() => alert('thanks for your purchase, we will get back to you shortly.\n\n -Henri Potier\'s sales team')}
            >
                <s.FullPrice>{booksPrice}$</s.FullPrice>
                <s.DiscountPrice>{cartPrice}$</s.DiscountPrice>
                <span>Click to Buy now !</span>
            </s.BuyButton>
        }
    </div>
);