import React from 'react'

import {fetchState} from "../../reducers/fetching";

import {StyledCartActionButton} from "../../styled/Buttons.style";
import {BuyButton} from '../../styled/BuyButton.style'
import EllipsisLoader from '../EllipsisLoader'


export default ({bookList, offersState, fetchOffers}) => {
    if(offersState === fetchState.PENDING)
        fetchOffers(bookList);
    return (
        <BuyButton
            style={{selfAlign: 'center'}}
            as={StyledCartActionButton}
        >
            <EllipsisLoader/>
        </BuyButton>
    )
}
