import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {RegularBookCard} from '../containers/Book'
import BookListHeader from '../containers/BookListHeader'

import * as s from '../styled/BookList.style'

export default class BookList extends Component {
    static propTypes = {
        fetchBooks: PropTypes.func.isRequired,
        bookList: PropTypes.arrayOf(PropTypes.string).isRequired,
    };

    constructor(props) {
        super(props);
        this.props.fetchBooks();
    }

    render() {
        return (
            <s.BookListContainer>
                <BookListHeader/>
                <s.BooksContainer>
                    { this.props.bookList.map(isbn => (<RegularBookCard key={isbn} isbn={isbn} />)) }
                </s.BooksContainer>
            </s.BookListContainer>
        )
    }
}