import styled from 'styled-components'

export const CartContainer = styled.div`
  margin-top: 90px;
  padding: 10px 20px;
  font-family: "Roboto", cursive;
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
`;