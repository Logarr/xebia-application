import styled from 'styled-components'

export const Container = styled.div`
  margin: 10px 20px;
  border: 1px solid #ddd;
  padding: 3px;
  border-radius: 2px;
  font-family: 'Roboto', sans-serif;
  width: 260px;
  cursor: pointer;
  &:hover {
    background: rgba(0, 0, 0, .1);
  }
`;

export const Header = styled.h1`
  text-align: center;
  font-size: 1em;
  font-family: 'Mali', cursive;
  text-overflow: ellipsis;
  overflow: hidden;
  max-width: 100%;
  white-space: nowrap;
`;

export const CoverContainer = styled.div`
  height: 380px;
  img {
    max-height: 100%;
  }
`;

export const Details = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 3px;
`;

export const Qty = styled.span`
  height: 20px;
`;

export const Price = styled.span`
  font-size: 20px;
  font-weight: bold;
`;

export const CartActions = styled.div`
  display: flex;
  text-align: center;
  align-items: center;
`;

export const CartButtons = styled.div`
  display: flex;
`;
