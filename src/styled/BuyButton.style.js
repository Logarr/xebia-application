import styled from 'styled-components'

export const BuyButton = styled.a`
  display: flex;
  flex-direction: column;
  align-self: center;
  font-family: "Roboto", cursive;
`;

export const FullPrice = styled.span`
  color: red;
  text-decoration: line-through;
  font-family: "Roboto", cursive;
`;


export const DiscountPrice = styled.span`
  color: white;
  font-weight: bold;
  font-family: "Roboto", cursive;
`;
