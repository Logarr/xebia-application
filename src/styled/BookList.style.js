import styled from 'styled-components'
import {headerHeight} from "./App.style";

export const BookListContainer = styled.div`
  margin-top: ${headerHeight};
  font-family: "Roboto", cursive;
`;

export const BooksContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  overflow-y: auto;
  height: 100%;
`;

export const HeaderContainer = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  label {
    font-size: 14px;
    font-weight: bold;
    margin-right: 10px;
  }
`;

export const FilterContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

export const TextSearch = styled.input`
    width: 30%;
    border: 1px solid #ddd;
    padding: 5px;
    height: 20px;
    border-radius: 5px;
    outline: none;
    font-family: "Roboto", cursive;
    text-align: center;
    color: #444;
    margin-bottom: 10px;
    font-size: 18px;
`;

export const SortContainer = styled.div`
    display: flex;
    justify-content: space-between;
    width: 30%;
    margin: 0 auto;
    input {
      width: 40px;
      border: 1px solid #ddd;
      text-align: center;
    }
    a {
      text-decoration: none;
      color: #fff;
      background-color: #2666a6;
      text-align: center;
      letter-spacing: .5px;
      transition: background-color .2s ease-out;
      cursor: pointer;
      border: none;
      border-radius: 2px;
      display: inline-block;
      height: 20px;
      line-height: 20px;
      font-size: 12px;
      padding: 0 6px;
      text-transform: uppercase;
      vertical-align: middle;
      box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
      margin-right: 10px;
      user-select: none;
      &:focus {
        background-color: #1d497d;
      }
      &:hover {
        background-color: #2b7abb;
      }
      &.asc:after {
        content: '\\25B2';
        padding-left: 6px;
      }
      &.desc:after {
        content: '\\25BC';
        padding-left: 6px;
      }
    }
`;