import styled from 'styled-components'

export const PendingCartSummary = styled.div`
  display: flex;
  flex-direction: column;
  a{
    font-size: 10px;
    background: rgba(255,255,255,.5);
    padding: 2px;
    border-radius: 2px;
    color: black;
    margin-right: 5px;
    &:hover {
      background: rgba(255,255,255,.8);
    }
  }
`;
