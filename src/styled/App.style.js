import styled from 'styled-components'

export const headerHeight = '90px';

export const Header = styled.div`
  position: fixed;
  top: 0;
  height: ${headerHeight};
  width: 100%;
  display: flex;
  background: white;
  border-bottom: 1px solid black;
  align-items: center;
  justify-content: space-between;
`;

export const Title = styled.h1`
  font-family: "Charmonman", cursive;
  width: 50%;
  text-align: center;
  a {
    color: darkblue;
    text-decoration: underline;
  }
`;

export const Cart = styled.div`
  margin-right: 20px;
  padding: .4em .7em;
  border-radius: 0.2em;
  box-sizing: border-box;
  text-decoration: none;
  font-family: 'Roboto',sans-serif;
  font-weight: 400;
  color: #FFFFFF;
  background-color: #2979FF;
  box-shadow: inset 0 -0.6em 1em -0.35em rgba(0,0,0,0.17), inset 0 0.6em 2em -0.3em rgba(255,255,255,0.15), inset 0 0 0em 0.05em rgba(255,255,255,0.12);
  text-align: center;
  position: relative;
  cursor: pointer;
  display: flex;
  align-items: center;
  > img {
    height: 20px;
  }
`;