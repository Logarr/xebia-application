import styled from 'styled-components'

export const ModalContainer = styled.div`
  position: fixed;
  z-index: 1;
  height: 100%;
  width: 100%;
  top: 0;
  background-color: rgba(0,0,0,0.4);
  overflow: auto;
`;

export const ModalContent = styled.div`
  background: whitesmoke;
  margin: 30px auto;
  padding: 20px;
  border: 1px solid #888;
  width: 650px;
  top: 0;
  left: 0;
  font-family: "Roboto",cursive;
  display: flex;
  flex-direction: column;
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`;

export const Title = styled.h1`
  font-family: "mali", cursive;
  flex-grow: 1;
  text-align: center;
  font-size: 1.7em;
`;

export const CloseButton = styled.a`
  font-weight: bold;
  cursor: pointer;
  border-radius: 50%;
  color: black;
  background: whitesmoke;
  padding: 7px 10px;
  &:hover {
    background: #ddd;
  }
`;

export const TopPanel = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Cover = styled.div`
  img {
    height: 300px;
  }
`;

export const Cart = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
`;

export const Price = styled.div`
  font-weight: bold;
`;

export const CartActions = styled.div`
  display: flex;
`;

export const Buttons = styled.div`
  display: flex;
`;

export const Synopsis = styled.div`
  margin-top: 24px;
`;