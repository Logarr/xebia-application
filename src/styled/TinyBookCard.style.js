import styled from 'styled-components'

export const TinyBookCardContainer = styled.div`
  display: flex;
  align-items: center;
  border: 1px solid #ddd;
  margin-bottom: 6px;
  flex-direction: column;
  cursor: pointer;
  &:hover {
    background: rgba(0, 0, 0, .1);
  }
`;

export const SummaryContainer = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
`;

export const DetailsContainer = styled.div`
  display: flex;
  align-items: center;
  margin-right: 10px;
`;

export const Title = styled.div`
    font-family: "Mali", cursive;
    text-align: center;
    text-decoration: underline;
`;

export const Qty = styled.div`
  align-items: center;
  text-align: center;
`;

export const Cover = styled.div`
  height: 65px;
  padding: 2px 10px;
  img {
    max-height: 100%;
  }
`;

export const CartContainer = styled.div`
   display: flex;
   flex-direction: column; 
   align-self: stretch;
   justify-content: space-between;
`;


export const CartActions = styled.div`
  display: flex;
  align-items: center;
`;

export const Price = styled.div`
  font-weight: bold;
`;

export const Synopsis = styled.div`
    font-family: "Roboto", cursive;
    font-size: 10px;
    font-style: italic;
`;