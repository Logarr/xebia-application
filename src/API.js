import Axios from 'axios'

const base_url = " http://henri-potier.xebia.fr";

export const booksRoute = () => `${base_url}/books`;
export function getBooks() {
    return Axios.get(booksRoute())
        .then(({data}) => {
            if(!Array.isArray(data))
                throw new Error("Invalid response for getBooks() : Expected array, but got something else");
            return data;
        });
}

export const commercialOffersRoute = (isbnsList) => `${base_url}/books/${isbnsList.join(',')}/commercialOffers`;
export function getCommercialOffers(isbnsList) {
    return Axios.get(commercialOffersRoute(isbnsList))
        .then(({data}) => {
            const offers = data.offers;
            if(!Array.isArray(offers))
                throw new Error("Invalid response for getOffers() : Expected response.offers to be an array, but got something else");
            return offers;
        });
}