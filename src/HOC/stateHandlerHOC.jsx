import React from 'react'
import {fetchState} from "../reducers/fetching";

const stateHandlerHOC = (stateProp, {LoadingComponent, FailureComponent, PendingComponent} = {}) => WrappedComponent => {
    return props => {
        LoadingComponent = LoadingComponent || WrappedComponent;
        FailureComponent = FailureComponent || WrappedComponent;
        PendingComponent = PendingComponent || WrappedComponent;

        switch(props[stateProp]) {
            case fetchState.FETCHING:
                return (<LoadingComponent { ...props }/>);
            case fetchState.FAILURE:
                return (<FailureComponent  { ...props }/>);
            case fetchState.PENDING:
                return (<PendingComponent { ...props }/>);
            default:
                return (<WrappedComponent { ...props }/>);
        }
    };
};

export default stateHandlerHOC;