import {connect} from 'react-redux'
import BookSelection from '../components/BookSelection'

const mapStateToProps = (state) => {
    const bookList = state.get('cart').keySeq().toJS();
    return {bookList};
};

export default connect(mapStateToProps)(BookSelection);