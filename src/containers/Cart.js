import {connect} from 'react-redux'
import {compose} from 'redux'

import CartSummaryComponent from '../components/CartSummary/CartSummary'
import PendingCartSummary from '../components/CartSummary/PendingCartSummary'
import LoadingCartSummary from '../components/CartSummary/LoadingCartSummary'
import {fetchCommercialOffers} from "../actions/offers";
import {getCartKey, getCartPrice, getBooksPrice} from "../entities/cart";
import {fetchState} from "../reducers/fetching";
import {convertToISBNList} from "../reducers/cart";
import stateHandlerHOC from '../HOC/stateHandlerHOC';
import { withRouter } from "react-router-dom"
import FailureBuyButton from "../components/BuyButton/FailureBuyButton";
import LoadingBuyButton from "../components/BuyButton/LoadingBuyButton";
import BuyButtonComponent from '../components/BuyButton/BuyButton'

const mapStateToProps = (state) => {
    const bookList = convertToISBNList(state.get('cart'));
    const cartKey = getCartKey(bookList);

    const books = bookList.map((isbn) => state.getIn(['books', isbn]).toJS());
    const offersState = state.getIn(['fetching', 'offers', cartKey]) || fetchState.PENDING;
    const booksPrice = getBooksPrice(books);
    let cartPrice = 0;
    if(offersState === fetchState.SUCCESS) {
        const offers = state.getIn(['offers', cartKey]);
        cartPrice = getCartPrice(offers, books);
    }
    return {bookList, cartPrice, offersState, booksPrice};
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchOffers: (bookList) => dispatch(fetchCommercialOffers(bookList))
    }
};

export const BuyButton = compose(
    connect(mapStateToProps, mapDispatchToProps),
    stateHandlerHOC(
        'offersState',
        {
            LoadingComponent: LoadingBuyButton,
            FailureComponent: FailureBuyButton,
            PendingComponent: LoadingBuyButton
        })
)(BuyButtonComponent);

export const CartSummary = withRouter(compose(
    connect(mapStateToProps, mapDispatchToProps),
    stateHandlerHOC(
        'offersState',
        {
            LoadingComponent: LoadingCartSummary,
            FailureComponent: PendingCartSummary,
            PendingComponent: PendingCartSummary
        })
)(CartSummaryComponent));