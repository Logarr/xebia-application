import {connect} from 'react-redux'
import RegularCard from '../components/BookCard'
import TinyCard from '../components/TinyBookCard'
import {addToCart, removeFromCart} from "../actions/carts";
import {openBook, closeBook} from "../actions/books";
import BookModalComponent from "../components/BookModal";

const mapStateToProps = (state, {isbn}) => {
    const book = state.getIn(['books', isbn]).toJS();
    const qty = state.getIn(['cart', isbn]) || 0;
    return { ...book, qty };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (isbn) => dispatch(addToCart(isbn)),
        removeFromCart: (isbn) => dispatch(removeFromCart(isbn)),
        openBook: (isbn) => dispatch(openBook(isbn)),
        closeBook: () => dispatch(closeBook())
    }
};

const BookContainerCreator = connect(mapStateToProps, mapDispatchToProps);

export const RegularBookCard = BookContainerCreator(RegularCard);
export const TinyBookCard = BookContainerCreator(TinyCard);
export const BookModal = BookContainerCreator(BookModalComponent);