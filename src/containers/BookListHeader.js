import {connect} from 'react-redux'
import BookListHeader from '../components/BookListHeader'
import {setTextFilter, setMaxPriceFilter, togglePriceSort, toggleTitleSort} from "../actions/bookList";

const mapStateToProps = (state) => {
    const filter = state.get('filter').toJS();
    const sort = state.get('sort').toJS();
    return { filter, sort };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setTextFilter: (value) => dispatch(setTextFilter(value)),
        setMaxPriceFilter: (value) => dispatch(setMaxPriceFilter(value)),
        togglePriceSort: () => dispatch(togglePriceSort()),
        toggleTitleSort: () => dispatch(toggleTitleSort())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BookListHeader);