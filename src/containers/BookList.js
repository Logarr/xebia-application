import {connect} from 'react-redux'
import BookList from '../components/BookList'
import {fetchBooks} from "../actions/books";
import {maxPriceFilter, priceSort, textFilter, titleSort} from "../entities/book";
import {defaultState as defaultFilterState} from "../reducers/filter";

/**
 * Takes an immutable bookList and the current filter state
 * Returns a new bookList filtered accordingly to the passed filter state
 */
export function getFilteredBookList(bookList, state) {
    if(state.get('text') !== defaultFilterState.get('text'))
        bookList = bookList.filter(textFilter(state.get('text')));
    if(state.get('maxPrice') !== defaultFilterState.get('maxPrice'))
        bookList = bookList.filter(maxPriceFilter(state.get('maxPrice')));
    return bookList;
}

/**
 * Takes an immutable bookList and the current sort state
 * Returns a new bookList sorted accordingly to the passed sort state
 */
export function getSortedBookList(bookList, state) {
    const order = state.get('order');
    switch (state.get('key')) {
        case 'title':
            return bookList.sort(titleSort(order));
        case 'price':
            return bookList.sort(priceSort(order));
        default:
            return bookList;
    }
}

const mapStateToProps = (state) => {
    const wholeBookList = state.get('books').toList();
    const filteredBookList = getFilteredBookList(wholeBookList, state.get('filter'));
    const sortedBookList = getSortedBookList(filteredBookList, state.get('sort'));
    const bookList = sortedBookList.map(({isbn}) => isbn).toJS();

    return { bookList };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchBooks: () => dispatch(fetchBooks()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BookList);