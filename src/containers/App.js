import {connect} from 'react-redux'
import App from '../components/App'

const mapStateToProps = (state) => {
    const openBook = state.get('openBook');
    return { openBook };
};

export default connect(mapStateToProps)(App);