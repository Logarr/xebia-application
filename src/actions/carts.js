export const ADD_TO_CART = 'ADD_TO_CART';
export function addToCart(isbn) {
    return {
        type: ADD_TO_CART,
        isbn,
    };
}

export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export function removeFromCart(isbn) {
    return {
        type: REMOVE_FROM_CART,
        isbn
    };
}