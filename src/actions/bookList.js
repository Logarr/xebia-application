export const TEXT_FILTER = 'TEXT_FILTER';
export function setTextFilter(value) {
    return {
        type: TEXT_FILTER,
        value
    }
}

export const MAX_PRICE_FILTER = 'MAX_PRICE_FILTER';
export function setMaxPriceFilter(value) {
    return {
        type: MAX_PRICE_FILTER,
        value
    }
}

export const TOGGLE_TITLE_SORT = 'TOGGLE_TITLE_SORT';
export function toggleTitleSort() {
    return { type: TOGGLE_TITLE_SORT }
}

export const TOGGLE_PRICE_SORT = 'TOGGLE_PRICE_SORT';
export function togglePriceSort() {
    return { type: TOGGLE_PRICE_SORT }
}