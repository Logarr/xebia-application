import {getBooks} from "../API";
import {createBook} from "../entities/book";
import {fetchState} from "../reducers/fetching";

export const FETCH_BOOKS = 'FETCH_BOOKS';
export function fetchBooks() {
    return (dispatch, getState) => {
        if(getState().getIn(['fetching', 'books']) === fetchState.SUCCESS)
            return Promise.resolve();
        dispatch({ type: FETCH_BOOKS });
        return getBooks()
            .then((books) => dispatch(fetchBooksSuccess(books.map(createBook))))
            .catch((err) => dispatch(fetchBooksFail(err)))
            .finally(() => dispatch(fetchBooksDone()));
    }
}

export const FETCH_BOOKS_SUCCESS = 'FETCH_BOOKS_SUCCESS';
function fetchBooksSuccess(books) {
    return { type: FETCH_BOOKS_SUCCESS, books };
}

export const FETCH_BOOKS_FAIL = 'FETCH_BOOKS_FAIL';
function fetchBooksFail(error) {
    return { type: FETCH_BOOKS_FAIL, error };
}

export const FETCH_BOOKS_DONE = 'FETCH_BOOKS_DONE';
function fetchBooksDone() {
    return { type: FETCH_BOOKS_DONE };
}

export const OPEN_BOOK = 'OPEN_BOOK';
export function openBook(isbn) {
    return { type: OPEN_BOOK, isbn };
}

export const CLOSE_BOOK = 'CLOSE_BOOK';
export function closeBook() {
    return { type: CLOSE_BOOK };
}