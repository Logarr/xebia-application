import * as API from '../API'
import {createOffer} from "../entities/offer"
import {getCartKey} from "../entities/cart";

export const FETCH_OFFERS = 'FETCH_OFFERS';
export function fetchCommercialOffers(isbnList) {
    return (dispatch) => {
        const key = getCartKey(isbnList);

        dispatch({ type: FETCH_OFFERS, key });
        return API.getCommercialOffers(isbnList)
            .then((offers) => dispatch(fetchOffersSuccess(offers.map(createOffer), key)))
            .catch((err) => dispatch(fetchOffersFail(err, key)))
            .finally(() => {dispatch(fetchOffersDone())});
    }
}

export const FETCH_OFFERS_SUCCESS = 'FETCH_OFFERS_SUCCESS';
function fetchOffersSuccess(offers, key) {
    return {
        type: FETCH_OFFERS_SUCCESS,
        offers,
        key
    };
}

export const FETCH_OFFERS_FAIL = 'FETCH_OFFERS_FAIL';
function fetchOffersFail(error, key) {
    return { type: FETCH_OFFERS_FAIL, error, key };
}

export const FETCH_OFFERS_DONE = 'FETCH_OFFERS_DONE';
function fetchOffersDone() {
    return { type: FETCH_OFFERS_DONE };
}