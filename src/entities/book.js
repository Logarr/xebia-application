import {List, Record} from 'immutable'

const Book = Record({isbn: '', title: '', price: 0, cover: '', synopsis: List()});

export const createBook = ({isbn, title, price, cover, synopsis}) =>
        Book({isbn, title, price, cover, synopsis: List(synopsis)});

/**
 * takes an order argument which must be one of 'asc' or 'desc'
 * and returns a book price sorting function to be passed to an array like sorting function.
 */
export const priceSort = (order) => {
    const factor = order === 'desc' ? -1 : 1;
    return ({price: a}, {price: b}) => (a - b) * factor;
};

/**
 * takes an order argument which must be one of 'asc' or 'desc'
 * and returns a book title sorting function to be passed to an array-like sorting function.
 */
export const titleSort = (order) => {
    const factor = order === 'desc' ? -1 : 1;
    return ({title: a}, {title: b}) => a.localeCompare(b, undefined, {sensitivity: 'base'}) * factor;
};

/**
 * each following function takes a value and return a book filtering function according to that value that is to be passed to an array-like filtering function
 */
export const maxPriceFilter = (value) => ({price}) => price <= value;
export const textFilter = (value) => ({title, synopsis}) => {
    const removeDiacritics = (s) => s.normalize('NFD').replace(/[\u0300-\u036f]/g, "");

    const normalizedValue = removeDiacritics(value).toLowerCase();
    const normalizedTitle = removeDiacritics(title).toLowerCase();
    if(normalizedTitle.includes(normalizedValue))
        return true;
    const normalizedSynopsis = removeDiacritics(synopsis.join(' ')).toLowerCase();
    return normalizedSynopsis.includes(normalizedValue);
};