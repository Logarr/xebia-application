import {List, Record} from 'immutable'
import {computeOfferPrice} from "./offer";

const Cart = Record({books: List()});

export const createCart = (books = []) => {
    return new Cart({
        books: List(books).map(({isbn}) => isbn)
    });
};

export const getCartKeyFromBooks = (books = []) => getCartKey(books.map(({isbn}) => isbn));

export const getCartKey = (isbnList) => `cart-[${[...isbnList].sort().join(',')}]`;

export const getBooksPrice = (books) => books.reduce((total, {price}) => total + price, 0);

export const getCartPrice = (offers, books) => {
    const totalPrice = getBooksPrice(books);
    return Math.min(totalPrice, ...offers.map((offer) => computeOfferPrice(offer)(totalPrice)));
};