import {Record} from 'immutable'

export const PERCENTAGE = 'percentage';
export const MINUS = 'minus';
export const SLICE = 'slice';

const offersTypes = Object.freeze({
    [PERCENTAGE]: ({value}) => (price) => Math.max(0, price - (price / 100 * value)),
    [MINUS]: ({value}) => (price) => Math.max(0, price - value),
    [SLICE]: ({value, sliceValue}) => (price) => Math.max(0, price - Math.floor(price / sliceValue) * value)
});

const Offer = Record({type: 'minus', value: 0, sliceValue: 0});

export const createOffer = ({type, value, sliceValue}) => {
    const supportedTypes = [PERCENTAGE, MINUS, SLICE];

    if(!supportedTypes.includes(type))
        throw new Error(`Unknown offer type, supported types are ${supportedTypes.join(', ')}`);

    if(type === SLICE && (typeof sliceValue !== "number" || !(sliceValue > 0)))
        throw new Error("sliceValue parameter must be a number greater than zero");

    return Offer({
        type,
        value,
        sliceValue,
    });
};

export const computeOfferPrice = ({type, value, sliceValue}) => offersTypes[type]({value, sliceValue});