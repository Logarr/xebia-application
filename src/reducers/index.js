import {Map} from 'immutable'

import books from "./books";
import offers from "./offers";
import fetching from "./fetching";
import cart from "./cart";
import openBook from './openBook';
import filter from './filter';
import sort from './sort';

export default (state = Map(), action) => {
    return Map({
        books: books(state.get('books'), action),
        cart: cart(state.get('cart'), action),
        offers: offers(state.get('offers') , action),
        fetching: fetching(state.get('fetching'), action),
        openBook: openBook(state.get('openBook'), action),
        filter: filter(state.get('filter'), action),
        sort: sort(state.get('sort'), action)
    });
};