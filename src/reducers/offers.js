import {Map} from 'immutable'
import {FETCH_OFFERS_SUCCESS} from "../actions/offers";

export default (state = Map(), {type, ...action}) => {
    switch (type) {
        case FETCH_OFFERS_SUCCESS:
            return state.set(action.key, action.offers);
        default:
            return state;
    }
}