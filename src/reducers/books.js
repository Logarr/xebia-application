import {Map} from 'immutable'

import {FETCH_BOOKS_SUCCESS} from '../../src/actions/books'

export default (state = Map(), {type, ...action}) => {
    switch (type) {
        case FETCH_BOOKS_SUCCESS:
            return Map(action.books.reduce((acc, book) => ({...acc, [book.isbn]: book}), {}));
        default:
            return state;
    }
}