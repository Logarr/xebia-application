import {Map} from 'immutable'

import {ADD_TO_CART, REMOVE_FROM_CART} from "../actions/carts";

export default (state = Map(), {type, ...action}) => {
    switch (type) {
        case ADD_TO_CART:
            return state.update(action.isbn, 0, (qty) => qty + 1);
        case REMOVE_FROM_CART:
            return removeFromCart(state, action.isbn);
        default:
            return state;
    }
}

function removeFromCart(state, isbn) {
    const qty = state.get(isbn);
    if(qty > 1)
        return state.update(isbn, (qty) => qty - 1);
    if(typeof qty === 'number')
        return state.delete(isbn);
    return state;
}

/**
 * Takes a cart state and returns a list of ISBNs for each book in the cart.
 * Example: {foo: 1, bar: 2} will return ['foo', 'bar', 'bar']
 */
export function convertToISBNList(state) {
    return state.reduce((acc, qty, isbn) => [...acc, ...Array(qty).fill(isbn)],[]);
}