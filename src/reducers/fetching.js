import {Map} from 'immutable'
import {FETCH_BOOKS, FETCH_BOOKS_FAIL, FETCH_BOOKS_SUCCESS} from "../actions/books";
import {FETCH_OFFERS, FETCH_OFFERS_FAIL, FETCH_OFFERS_SUCCESS} from "../actions/offers";

export const fetchState = Object.freeze({
    PENDING: 'PENDING',
    FETCHING: 'FETCHING',
    SUCCESS: 'SUCCESS',
    FAILURE: 'FAILURE'
});

export default (state = Map({books: fetchState.PENDING, offers: Map()}), {type, ...action}) => {
    switch (type) {
        case FETCH_BOOKS:
            return state.set('books', fetchState.FETCHING);
        case FETCH_BOOKS_SUCCESS:
            return state.set('books', fetchState.SUCCESS);
        case FETCH_BOOKS_FAIL:
            return state.set('books', fetchState.FAILURE);
        case FETCH_OFFERS:
            return state.setIn(['offers', action.key], fetchState.FETCHING);
        case FETCH_OFFERS_SUCCESS:
            return state.setIn(['offers', action.key], fetchState.SUCCESS);
        case FETCH_OFFERS_FAIL:
            return state.setIn(['offers', action.key], fetchState.FAILURE);
        default:
            return state;
    }
}