import {Map} from 'immutable'

import { TEXT_FILTER, MAX_PRICE_FILTER } from '../actions/bookList'

export const defaultState = Map({
    text: '',
    maxPrice: Infinity
});

export default (state = defaultState, {type, ...action}) => {
    switch(type) {
        case TEXT_FILTER:
            return state.set('text', action.value);
        case MAX_PRICE_FILTER:
            return state.set('maxPrice', Math.max(0, action.value) || defaultState.get('maxPrice')); //We don't allow a 0 value here, because it basically means that we want to reset the maxPrice filter
        default:
            return state;
    }
};