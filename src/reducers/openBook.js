import {CLOSE_BOOK, OPEN_BOOK} from "../actions/books";

export default (state = null, {type, ...action}) => {
    switch(type) {
        case OPEN_BOOK:
            return action.isbn;
        case CLOSE_BOOK:
            return null;
        default:
            return state;
    }
}