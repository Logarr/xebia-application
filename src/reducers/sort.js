import {Map} from 'immutable'

import { TOGGLE_TITLE_SORT, TOGGLE_PRICE_SORT } from '../actions/bookList'

export default (state = Map({key: null, order: 'asc'}), {type}) => {
    switch(type) {
        case TOGGLE_TITLE_SORT:
            return handleToggle(state, 'title');
        case TOGGLE_PRICE_SORT:
            return handleToggle(state, 'price');
        default:
            return state;
    }
};

function handleToggle(state, key) {
    if(state.get('key') !== key)
        return state
            .set('key', key)
            .set('order', 'asc');
    switch(state.get('order')) {
        case 'asc':
            return state.set('order', 'desc');
        case 'desc':
            return state
                .set('key', null)
                .set('order', 'asc');
        default:
            return state
                .set('key', key)
                .set('order', 'asc');
    }
}