import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import reducer from './reducers'
import App from './containers/App'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import {Map} from 'immutable';

const loggerMiddleware = createLogger({
    stateTransformer: (state) => state.toJS()
});

const devMiddleWares = [
    loggerMiddleware
];

const middleWares = [
    thunk
];

if (process.env.NODE_ENV !== 'production') {
    middleWares.push(...devMiddleWares);
}

const store = createStore(reducer, Map(), applyMiddleware(...middleWares));

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);