Henri Potier's Bookshop
==

Prepare it
--

- install [npm](https://www.npmjs.com/)

Install it
--
`npm install && npm install webpack-dev-server -g && npm install jest -g`

Use it
--

**Test it**

run `npm run test` to launch the tests

**Run it**

- Make sure you have a working internet connection

- Run `npm run start` to launch webpack-dev-server on port 3000

- Visit [http://localhost:3000]() and enjoy Henri Potier's Bookshop