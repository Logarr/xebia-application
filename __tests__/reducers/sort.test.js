import {Map} from 'immutable'
import rootReducer from '../../src/reducers'
import {TOGGLE_PRICE_SORT, TOGGLE_TITLE_SORT} from "../../src/actions/bookList";

describe('sort reducer', () => {
    describe('empty state', () => {
        it('handles TOGGLE_PRICE_SORT', () => {
            const state = Map();
            const action = { type: TOGGLE_PRICE_SORT };

            const nextState = rootReducer(state, action);

            expect(nextState.get('sort')).toEqual(Map({key: 'price', order: 'asc'}));
        });

        it('handles TOGGLE_TITLE_SORT', () => {
            const state = Map();
            const action = { type: TOGGLE_TITLE_SORT };

            const nextState = rootReducer(state, action);

            expect(nextState.get('sort')).toEqual(Map({key: 'title', order: 'asc'}));
        });
    });

    describe('existing state with different key', () => {
        it('just replaces the existing key and sets an asc order for TOGGLE_PRICE_SORT', () => {
            const state = Map({sort: Map({key: 'title', order: 'desc'})});
            const action = { type: TOGGLE_PRICE_SORT };

            const nextState = rootReducer(state, action);

            expect(nextState.get('sort')).toEqual(Map({key: 'price', order: 'asc'}));
        });

        it('just replaces the existing key and sets an asc order for TOGGLE_TITLE_SORT', () => {
            const state = Map({sort: Map({key: 'price', order: 'asc'})});
            const action = { type: TOGGLE_TITLE_SORT };

            const nextState = rootReducer(state, action);

            expect(nextState.get('sort')).toEqual(Map({key: 'title', order: 'asc'}));
        });
    });

    describe('existing state with same key', () => {
        it('sets the next order to desc when the current order is asc', () => {
            const state = Map({sort: Map({key: 'price', order: 'asc'})});
            const action = { type: TOGGLE_PRICE_SORT };

            const nextState = rootReducer(state, action);

            expect(nextState.get('sort')).toEqual(Map({key: 'price', order: 'desc'}));
        });

        it('removes the sort when the current order is desc', () => {
            const state = Map({sort: Map({key: 'title', order: 'desc'})});
            const action = { type: TOGGLE_TITLE_SORT };

            const nextState = rootReducer(state, action);

            expect(nextState.get('sort')).toEqual(Map({key: null, order: 'asc'}));
        });

        it('defaults to a new sort when the current order is unknown', () => {
            const state = Map({sort: Map({key: 'title', order: 'foobar'})});
            const action = { type: TOGGLE_TITLE_SORT };

            const nextState = rootReducer(state, action);

            expect(nextState.get('sort')).toEqual(Map({key: 'title', order: 'asc'}));
        })
    });
});