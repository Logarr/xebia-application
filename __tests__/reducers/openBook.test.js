import {Map} from 'immutable'
import rootReducer from '../../src/reducers'
import {OPEN_BOOK, CLOSE_BOOK} from "../../src/actions/books";

describe('books reducer', () => {
    it('handles OPEN_BOOK', () => {
        const state = Map();
        const action = {
            type: OPEN_BOOK,
            isbn: 'foo'
        };

        const nextState = rootReducer(state, action);

        expect(nextState.get('openBook')).toEqual('foo');
    });

    it('handles CLOSE_BOOK', () => {
        const state = Map();
        const action = { type: CLOSE_BOOK };

        const nextState = rootReducer(state, action);

        expect(nextState.get('openBook')).toBeNull();
    });
});