import {Map} from 'immutable'
import rootReducer from '../../src/reducers'
import {ADD_TO_CART, REMOVE_FROM_CART} from "../../src/actions/carts";
import {convertToISBNList} from "../../src/reducers/cart";

describe('carts reducer', () => {
    it('handles ADD_TO_CART for a non existing key', () => {
        const isbn = 'foo';
        const action = {
            type: ADD_TO_CART,
            isbn
        };

        const state = Map();
        const nextState = rootReducer(state, action);

        const expectedState = Map({[isbn]: 1});

        expect(nextState.get('cart')).toEqual(expectedState);
    });

    it('handles ADD_TO_CART for an existing key', () => {
        const isbn = 'foo';
        const action = {
            type: ADD_TO_CART,
            isbn
        };

        const state = Map({cart: Map({[isbn]: 3})});
        const nextState = rootReducer(state, action);

        const expectedState = Map({[isbn]: 4});

        expect(nextState.get('cart')).toEqual(expectedState);
    });

    it('handles REMOVE_FROM_CART for an existing key', () => {
        const isbn = 'foo';
        const action = {
            type: REMOVE_FROM_CART,
            isbn
        };

        const state = Map({cart: Map({[isbn]: 2})});
        const nextState = rootReducer(state, action);

        const expectedState = Map({[isbn]: 1});

        expect(nextState.get('cart')).toEqual(expectedState);
    });

    it('removes the key if the qty reaches 0', () => {
        const isbn = 'foo';
        const action = {
            type: REMOVE_FROM_CART,
            isbn
        };

        const state = Map({cart: Map({[isbn]: 1})});
        const nextState = rootReducer(state, action);

        const expectedState = Map();

        expect(nextState.get('cart')).toEqual(expectedState);
    });

    it('returns the original state if the specified isbn is not in the list', () => {
        const action = {
            type: REMOVE_FROM_CART,
            isbn: 'bar'
        };

        const state = Map({cart: Map({foo: 1})});
        const nextState = rootReducer(state, action);

        expect(nextState.get('cart')).toEqual(state.get('cart'));
    });

    it('converts a cart state to a bookList with respect of quantities', () => {
        const state = Map({foo: 1, bar: 2});
        expect(convertToISBNList(state)).toEqual(['foo', 'bar', 'bar']);
    });
});