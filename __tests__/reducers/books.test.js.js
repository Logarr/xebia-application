import {Map} from 'immutable'
import rootReducer from '../../src/reducers'
import {FETCH_BOOKS_SUCCESS} from "../../src/actions/books";
import {rawBooks} from "../entities/fixtures/books";
import {createBook} from "../../src/entities/book";

describe('books reducer', () => {
    it('feeds the books state properly on FETCH_BOOKS_SUCCESS', () => {
        const state = Map();
        const books = rawBooks().map(createBook);
        const action = {
            type: FETCH_BOOKS_SUCCESS,
            books: books
        };

        const nextState = rootReducer(state, action);

        const booksMap = Map(books.reduce((acc, book) => ({...acc, [book.isbn]: book}), {}));
        expect(nextState.get('books')).toEqual(booksMap);
    });
});