import {Map} from 'immutable'
import rootReducer from '../../src/reducers'
import {TEXT_FILTER, MAX_PRICE_FILTER} from "../../src/actions/bookList";

describe('filter reducer', () => {
    it('handles TEXT_FILTER', () => {
        const state = Map();
        const action = {
            type: TEXT_FILTER,
            value: 'foobar'
        };

        const nextState = rootReducer(state, action);

        expect(nextState.getIn(['filter', 'text'])).toBe('foobar');
    });

    it('handles MAX_PRICE_FILTER', () => {
        const state = Map();
        const action = {
            type: MAX_PRICE_FILTER,
            value: 60
        };

        const nextState = rootReducer(state, action);

        expect(nextState.getIn(['filter', 'maxPrice'])).toBe(60);
    });

    it('resets maxPrice to Infinity if a value <= 0 is provided', () => {
        const state = Map();
        const maxPriceAction = {
            type: MAX_PRICE_FILTER,
            value: 0
        };

        const nextState = rootReducer(state, maxPriceAction);

        expect(nextState.getIn(['filter', 'maxPrice'])).toBe(Infinity);
    });
});