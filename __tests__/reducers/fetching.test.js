import {Map} from 'immutable'
import rootReducer from '../../src/reducers'
import {FETCH_BOOKS, FETCH_BOOKS_SUCCESS, FETCH_BOOKS_FAIL} from "../../src/actions/books";
import {FETCH_OFFERS, FETCH_OFFERS_FAIL, FETCH_OFFERS_SUCCESS} from "../../src/actions/offers";
import {fetchState} from "../../src/reducers/fetching";

describe('fetching reducer', () => {
    describe('books', () => {
        it('initializes with a book property set to fetchState.NONE', () => {
            const state = Map();
            const action = {
                type: undefined
            };

            const nextState = rootReducer(state, action);

            expect(nextState.getIn(['fetching', 'books'])).toEqual(fetchState.PENDING);
        });

        it('sets the book state to fetchState.FETCHING on FETCH_BOOKS', () => {
            const state = Map();
            const action = {
                type: FETCH_BOOKS
            };

            const nextState = rootReducer(state, action);

            expect(nextState.getIn(['fetching', 'books'])).toEqual(fetchState.FETCHING);
        });

        it('sets the book state to fetchState.SUCCESS on FETCH_BOOKS_SUCCESS', () => {
            const state = Map();
            const action = {
                type: FETCH_BOOKS_SUCCESS,
                books: []
            };

            const nextState = rootReducer(state, action);

            expect(nextState.getIn(['fetching', 'books'])).toEqual(fetchState.SUCCESS);
        });

        it('sets the book state to fetchState.FAILURE on FETCH_BOOKS_FAIL', () => {
            const state = Map();
            const action = {
                type: FETCH_BOOKS_FAIL
            };

            const nextState = rootReducer(state, action);

            expect(nextState.getIn(['fetching', 'books'])).toEqual(fetchState.FAILURE);
        });
    });

    describe('offers', () => {
        it('intializes with an empty map', () => {
            const state = Map();
            const action = {
                type: undefined
            };

            const nextState = rootReducer(state, action);

            expect(nextState.getIn(['fetching', 'offers'])).toEqual(Map());
        });

        it('sets the offer to fetchState.FETCHING on FETCH_OFFERS', () => {
            const state = Map();
            const action = {
                type: FETCH_OFFERS,
                key: 'foobar'
            };

            const nextState = rootReducer(state, action);

            expect(nextState.getIn(['fetching', 'offers', 'foobar'])).toEqual(fetchState.FETCHING);
        });

        it('sets the book state to fetchState.SUCCESS on FETCH_BOOKS_SUCCESS', () => {
            const state = Map();
            const action = {
                type: FETCH_OFFERS_SUCCESS,
                key: 'foobar',
                offers: [],
            };

            const nextState = rootReducer(state, action);

            expect(nextState.getIn(['fetching', 'offers', 'foobar'])).toEqual(fetchState.SUCCESS);
        });

        it('sets the book state to fetchState.FAILURE on FETCH_BOOKS_FAIL', () => {
            const state = Map();
            const action = {
                type: FETCH_OFFERS_FAIL,
                key: 'foobar'
            };

            const nextState = rootReducer(state, action);

            expect(nextState.getIn(['fetching', 'offers', 'foobar'])).toEqual(fetchState.FAILURE);
        });
    })
});