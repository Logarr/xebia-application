import moxios from "moxios"
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'
import {commercialOffersRoute} from "../../src/API";
import {createBook} from "../../src/entities/book";
import {rawBooks} from "../entities/fixtures/books";
import {rawOffers} from "../entities/fixtures/offers";
import {
    FETCH_OFFERS,
    FETCH_OFFERS_DONE,
    FETCH_OFFERS_FAIL,
    FETCH_OFFERS_SUCCESS,
    fetchCommercialOffers
} from "../../src/actions/offers";
import {createOffer} from "../../src/entities/offer";
import {getCartKeyFromBooks} from "../../src/entities/cart";
import {Map} from 'immutable'

describe('offers actions', () => {
    const middleWares = [thunk];
    const mockStore = configureStore(middleWares);
    let store;
    let books;

    beforeAll(() => {
        require('promise.prototype.finally').shim();
    });

    beforeEach(() => {
        store = mockStore(Map());
        moxios.install();
        moxios.delay = 1;
        books = rawBooks().map(createBook);
    });

    it('dispatches all needed actions for a success getCommercialOffers', () => {
        const bookList = books.map(({isbn}) => isbn);
        moxios.stubRequest(commercialOffersRoute(bookList), {
            status: 200,
            response: rawOffers()
        });

        return store.dispatch(fetchCommercialOffers(bookList)).then(() => {
            const expectedActionTypes = [FETCH_OFFERS, FETCH_OFFERS_SUCCESS, FETCH_OFFERS_DONE];
            expect(store.getActions().map(({type}) => type)).toEqual(expectedActionTypes);
        });
    });

    it('dispatches all needed actions for a failed getCommercialOffers', () => {
        const bookList = books.map(({isbn}) => isbn);
        moxios.stubRequest(commercialOffersRoute(bookList), {
            status: 500,
            response: {message: "Crashed while computing offers"}
        });

        return store.dispatch(fetchCommercialOffers(bookList)).then(() => {
            const expectedActionTypes = [FETCH_OFFERS, FETCH_OFFERS_FAIL, FETCH_OFFERS_DONE];
            expect(store.getActions().map(({type}) => type)).toEqual(expectedActionTypes);
        });
    });

    it('creates a list of offers for a standard response', () => {
        const bookList = books.map(({isbn}) => isbn);
        moxios.stubRequest(commercialOffersRoute(bookList), {
            status: 200,
            response: rawOffers()
        });

        return store.dispatch(fetchCommercialOffers(bookList)).then(() => {
            const expectedAction = {
                type: FETCH_OFFERS_SUCCESS,
                offers: rawOffers().offers.map(createOffer),
                key: getCartKeyFromBooks(books)
            };
            expect(store.getActions().find(({type}) => type === FETCH_OFFERS_SUCCESS)).toEqual(expectedAction);
        });
    });

    afterEach(() => {
        moxios.uninstall();
    })
});