import {
    TOGGLE_PRICE_SORT, TOGGLE_TITLE_SORT, MAX_PRICE_FILTER, TEXT_FILTER,
    togglePriceSort, toggleTitleSort, setMaxPriceFilter, setTextFilter
} from "../../src/actions/bookList";

describe('bookList actions', () => {
    it('returns the expected action for toggleTitleSort', () => {
        const expectedAction = { type: TOGGLE_TITLE_SORT };

        expect(toggleTitleSort()).toEqual(expectedAction);
    });

    it('returns the expected action for togglePriceSort', () => {
        const expectedAction = { type: TOGGLE_PRICE_SORT };

        expect(togglePriceSort()).toEqual(expectedAction);
    });

    it('returns the expected action for setMaxPriceFilter', () => {
        const expectedAction = {
            type: MAX_PRICE_FILTER,
            value: 60
        };

        expect(setMaxPriceFilter(60)).toEqual(expectedAction);
    });

    it('returns the expected action for setTextFilter', () => {
        const expectedAction = {
            type: TEXT_FILTER,
            value: 'foo'
        };

        expect(setTextFilter('foo')).toEqual(expectedAction);
    });
});