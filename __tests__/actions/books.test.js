import moxios from "moxios"
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'
import {
    CLOSE_BOOK,
    closeBook,
    FETCH_BOOKS,
    FETCH_BOOKS_DONE,
    FETCH_BOOKS_FAIL,
    FETCH_BOOKS_SUCCESS,
    fetchBooks, OPEN_BOOK, openBook
} from "../../src/actions/books";
import {booksRoute} from "../../src/API";
import {rawBooks} from "../entities/fixtures/books";
import {createBook} from "../../src/entities/book";
import {Map} from 'immutable'
import {fetchState} from "../../src/reducers/fetching";

describe('books actions', () => {
    it('dispatches the right action for openBook', () => {
        const isbn = 'foobar';
        const expectedAction = {
            type: OPEN_BOOK,
            isbn
        };

        expect(openBook(isbn)).toEqual(expectedAction);
    });

    it('dispatches the right action for closeBook', () => {
        const isbn = 'foobar';
        const expectedAction = { type: CLOSE_BOOK };

        expect(closeBook(isbn)).toEqual(expectedAction);
    });

    describe('async', () => {
        const middleWares = [thunk];
        const mockStore = configureStore(middleWares);
        let store;

        beforeAll(() => {
            require('promise.prototype.finally').shim();
        });

        beforeEach(() => {
            store = mockStore(Map());
            moxios.install();
            moxios.delay = 1;
        });

        it('dispatches all needed actions for a success fetchBooks', () => {
            moxios.stubRequest(booksRoute(), {
                status: 200,
                response: rawBooks()
            });

            return store.dispatch(fetchBooks()).then(() => {
                const expectedActionTypes = [FETCH_BOOKS, FETCH_BOOKS_SUCCESS, FETCH_BOOKS_DONE];
                expect(store.getActions().map(({type}) => type)).toEqual(expectedActionTypes);
            });
        });

        it('does not dispatch anything if the books have already been fetched', () => {
            store = mockStore(Map({
                fetching: Map({
                    books: fetchState.SUCCESS
                })
            }));
            return store.dispatch(fetchBooks()).then(() => {
                expect(store.getActions().length).toBe(0);
            });
        });

        it('dispatches all needed actions for a failed fetchBooks', () => {
            moxios.stubRequest(booksRoute(), {
                status: 500,
                response: {message: "He-Who-Must-Not-Be-Named just stole all the books"}
            });

            return store.dispatch(fetchBooks()).then(() => {
                const expectedActionTypes = [FETCH_BOOKS, FETCH_BOOKS_FAIL, FETCH_BOOKS_DONE];
                expect(store.getActions().map(({type}) => type)).toEqual(expectedActionTypes);
            });
        });

        it('creates a list of books for a standard response', () => {
            moxios.stubRequest(booksRoute(), {
                status: 200,
                response: rawBooks()
            });

            return store.dispatch(fetchBooks()).then(() => {
                const expectedAction = {
                    type: FETCH_BOOKS_SUCCESS,
                    books: rawBooks().map(createBook)
                };
                expect(store.getActions().find(({type}) => type === FETCH_BOOKS_SUCCESS)).toEqual(expectedAction);
            });
        });

        afterEach(() => {
            moxios.uninstall();
        })
    });
});