import {ADD_TO_CART, addToCart, REMOVE_FROM_CART, removeFromCart} from "../../src/actions/carts";

describe('carts actions', () => {
    it('returns the expected action for addToCart', () => {
        const expectedAction = {
            type: ADD_TO_CART,
            isbn: 'foobar'
        };

        expect(addToCart('foobar')).toEqual(expectedAction);
    });

    it('returns the expected action for removeFromCart', () => {
        const expectedAction = {
            type: REMOVE_FROM_CART,
            isbn: 'foobar'
        };

        expect(removeFromCart('foobar')).toEqual(expectedAction);
    });
});