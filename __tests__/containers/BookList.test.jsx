import Adapter from "enzyme-adapter-react-16/build/index";
import {createMockStore} from "redux-test-utils";
import React from 'react';
import Enzyme from "enzyme";
import {Map} from 'immutable';
import { Provider } from 'react-redux'


import BookList, {getFilteredBookList, getSortedBookList} from '../../src/containers/BookList'

import {rawBooks} from "../entities/fixtures/books";
import {createBook} from "../../src/entities/book";
import {defaultState as defaultFilterState} from "../../src/reducers/filter";

Enzyme.configure({ adapter: new Adapter() });

describe('BookList Container', () => {
    let container;
    let state;
    let store;
    let books;

    beforeAll(() => {
        books = rawBooks().map(createBook);
        state = Map({
            books: Map({
                [books[0].isbn]: books[0],
                [books[1].isbn]: books[1],
            }),
            filter: Map({text: '', maxPrice: Infinity}),
            sort: Map({key: null, order: 'asc'}),
        });

        store = createMockStore(state);
        container = Enzyme.mount(
            <Provider store={store}>
                <BookList />
            </Provider>
        ).children()
    });

    it('gets the right bookList', () => {
        expect(container.children().props().bookList).toEqual(rawBooks().map(({isbn}) => isbn));
    });

    describe('actions', () => {
        const cartActions = require("../../src/actions/books");

        beforeAll(() => {
            cartActions.fetchBooks = jest.fn();
        });

        it('dispatches the right action when calling the fetchBooks property', () => {
            container.children().props().fetchBooks();
            expect(cartActions.fetchBooks).toHaveBeenCalled();
        });

        afterAll(() => {
            cartActions.fetchBooks.mockRestore();
        })
    });

    describe('books filter', () => {
        const book = require("../../src/entities/book");
        let books;

        beforeEach(() => {
            book.textFilter = jest.fn(() => () => true);
            book.maxPriceFilter = jest.fn(() => () => true);
            books = rawBooks().map(createBook);
        });

        it('does not call any filter if all the filters are at their default value', () => {
            const state = defaultFilterState;
            getFilteredBookList(books, state);
            expect(book.textFilter).not.toHaveBeenCalled();
            expect(book.maxPriceFilter).not.toHaveBeenCalled();
        });

        it('calls textFilter if the text filter has some value', () => {
            const state = defaultFilterState.set('text', 'foobar');
            getFilteredBookList(books, state);
            expect(book.textFilter).toHaveBeenCalled();
            expect(book.maxPriceFilter).not.toHaveBeenCalled();
        });

        it('calls maxPriceFilter if the maxPrice filter has some value', () => {
            const state = defaultFilterState.set('maxPrice', 20);
            getFilteredBookList(books, state);
            expect(book.maxPriceFilter).toHaveBeenCalled();
            expect(book.textFilter).not.toHaveBeenCalled();
        });

        it('calls both filters if they are both set in the filters', () => {
            const state = defaultFilterState.set('text', 'foobar').set('maxPrice', 20);
            getFilteredBookList(books, state);
            expect(book.maxPriceFilter).toHaveBeenCalled();
            expect(book.textFilter).toHaveBeenCalled();
        });

        afterEach(() => {
            book.textFilter.mockRestore();
            book.maxPriceFilter.mockRestore();
        });
    });

    describe('books sort', () => {
        const book = require("../../src/entities/book");
        let books;

        beforeEach(() => {
            book.titleSort = jest.fn(() => () => 1);
            book.priceSort = jest.fn(() => () => 1);
            books = rawBooks().map(createBook);
        });

        it('does not call any sort the sort key is null', () => {
            const state = Map({key: null, order: 'asc'});
            getSortedBookList(books, state);
            expect(book.titleSort).not.toHaveBeenCalled();
            expect(book.priceSort).not.toHaveBeenCalled();
        });

        it('calls a title sort with the right argument if the sort key is title', () => {
            const state = Map({key: 'title', order: 'desc'});
            getSortedBookList(books, state);
            expect(book.titleSort).toHaveBeenCalled();
            expect(book.titleSort.mock.calls[0][0]).toBe('desc');
            expect(book.priceSort).not.toHaveBeenCalled();
        });

        it('calls a price sort with the right argument if the sort key is price', () => {
            const state = Map({key: 'price', order: 'asc'});
            getSortedBookList(books, state);
            expect(book.priceSort).toHaveBeenCalled();
            expect(book.priceSort.mock.calls[0][0]).toBe('asc');
            expect(book.titleSort).not.toHaveBeenCalled();
        });

        afterEach(() => {
            book.titleSort.mockRestore();
            book.priceSort.mockRestore();
        });
    });
});