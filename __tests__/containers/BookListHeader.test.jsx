import Adapter from "enzyme-adapter-react-16/build/index";
import {createMockStore} from "redux-test-utils";
import React from 'react';
import Enzyme from "enzyme";
import {Map} from 'immutable';
import { Provider } from 'react-redux'


import BookListHeader from '../../src/containers/BookListHeader'

Enzyme.configure({ adapter: new Adapter() });

describe('BookList Container', () => {
    let container;
    let state;
    let store;

    beforeAll(() => {
        state = Map({
            filter: Map({text: 'foo', maxPrice: 20}),
            sort: Map({key: 'title', order: 'desc'}),
        });

        store = createMockStore(state);
        container = Enzyme.mount(
            <Provider store={store}>
                <BookListHeader />
            </Provider>
        ).children()
    });

    it('gets the right filter', () => {
        expect(container.children().props().filter).toEqual({text: 'foo', maxPrice: 20});
    });

    it('gets the right sort', () => {
        expect(container.children().props().sort).toEqual({key: 'title', order: 'desc'});
    });

    describe('actions', () => {
        const bookListActions = require("../../src/actions/bookList");

        beforeAll(() => {
            bookListActions.setTextFilter = jest.fn();
            bookListActions.setMaxPriceFilter = jest.fn();
            bookListActions.togglePriceSort = jest.fn();
            bookListActions.toggleTitleSort = jest.fn();
        });

        it('dispatches the right action when calling the setText property', () => {
            container.children().props().setTextFilter('foo');
            expect(bookListActions.setTextFilter).toHaveBeenCalled();
        });

        it('dispatches the right action when calling the setMaxPriceFilter property', () => {
            container.children().props().setMaxPriceFilter(20);
            expect(bookListActions.setMaxPriceFilter).toHaveBeenCalled();
        });

        it('dispatches the right action when calling the togglePriceSort property', () => {
            container.children().props().togglePriceSort();
            expect(bookListActions.togglePriceSort).toHaveBeenCalled();
        });

        it('dispatches the right action when calling the setText property', () => {
            container.children().props().toggleTitleSort();
            expect(bookListActions.toggleTitleSort).toHaveBeenCalled();
        });

        afterAll(() => {
            bookListActions.setTextFilter.mockRestore();
            bookListActions.setMaxPriceFilter.mockRestore();
            bookListActions.togglePriceSort.mockRestore();
            bookListActions.toggleTitleSort.mockRestore();
        })
    });
});