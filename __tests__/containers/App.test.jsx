import Adapter from "enzyme-adapter-react-16/build/index";
import {createMockStore} from "redux-test-utils";
import React from 'react';
import Enzyme from "enzyme";
import {Map} from 'immutable';

import {rawBooks} from "../entities/fixtures/books";
import {createBook} from "../../src/entities/book";
import App from '../../src/containers/App';
import { Provider } from 'react-redux';

Enzyme.configure({ adapter: new Adapter() });

describe('BooksSelection Container', () => {
    let container;
    let state;
    let store;
    let books;

    beforeAll(() => {
        books = rawBooks().map(createBook);
        state = Map({
            books: Map({
                [books[0].isbn]: books[0],
                [books[1].isbn]: books[1],
            }),
            cart: Map({
                [books[0].isbn]: 1,
                [books[1].isbn]: 2
            }),
            offers: Map({}),
            openBook: books[0].isbn,
            filter: Map({text: 'foo', maxPrice: 20}),
            sort: Map({key: 'title', order: 'desc'}),
        });

        store = createMockStore(state);
        container = Enzyme.mount(
            <Provider store={store}>
                <App />
            </Provider>
        ).children();
    });

    it('gets the right open book', () => {
        expect(container.children().props().openBook).toEqual(books[0].isbn);
    });
});