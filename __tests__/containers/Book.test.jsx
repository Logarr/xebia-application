import Adapter from "enzyme-adapter-react-16/build/index";
import {createMockStore} from "redux-test-utils";
import React from 'react';
import Enzyme from "enzyme";
import {Map} from 'immutable';

import {rawBooks} from "../entities/fixtures/books";
import {createBook} from "../../src/entities/book";
import {RegularBookCard} from '../../src/containers/Book';

Enzyme.configure({ adapter: new Adapter() });

describe('Books Container', () => {
    let container;
    let state;
    let store;
    let books;

    beforeAll(() => {
        books = rawBooks().map(createBook);
        state = Map({
            books: Map({
                [books[0].isbn]: books[0],
                [books[1].isbn]: books[1],
            }),
            cart: Map({
                [books[0].isbn]: 1,
                [books[1].isbn]: 2
            })
        });

        store = createMockStore(state);
        container = Enzyme.mount(
            <RegularBookCard store={store} isbn={books[1].isbn}/>
        )
    });

    it('gets the right book', () => {
        expect(container.children().props().isbn).toBe(books[1].isbn);
        expect(container.children().props().title).toBe(books[1].title);
    });

    it('gets the right quantity', () => {
        expect(container.children().props().qty).toBe(2);
    });

    describe('actions', () => {
        const cartActions = require("../../src/actions/carts");
        const booksAction = require("../../src/actions/books");

        beforeAll(() => {
            cartActions.addToCart = jest.fn();
            cartActions.removeFromCart = jest.fn();
            booksAction.openBook = jest.fn();
            booksAction.closeBook = jest.fn();
        });

        it('dispatches the right action when calling the addToCart property', () => {
            container.children().props().addToCart();
            expect(cartActions.addToCart).toHaveBeenCalled();
        });

        it('dispatches the right action when calling the removeFromCart property', () => {
            container.children().props().removeFromCart();
            expect(cartActions.removeFromCart).toHaveBeenCalled();
        });

        it('dispatches the right action when calling the openBook property', () => {
            container.children().props().openBook();
            expect(booksAction.openBook).toHaveBeenCalled();
        });

        it('dispatches the right action when calling the closeBook property', () => {
            container.children().props().closeBook();
            expect(booksAction.closeBook).toHaveBeenCalled();
        });

        afterAll(() => {
            cartActions.addTocart.mockRestore();
            cartActions.removeFromCart.mockRestore();
            booksAction.openBook.mockRestore();
            booksAction.closeBook.mockRestore();
        })
    });
});