import Adapter from "enzyme-adapter-react-16/build/index";
import {createMockStore} from "redux-test-utils";
import React from 'react';
import Enzyme from "enzyme";
import {Map, Set} from 'immutable';

import {rawBooks} from "../entities/fixtures/books";
import {createBook} from "../../src/entities/book";
import {rawOffers} from "../entities/fixtures/offers";
import {createOffer} from '../../src/entities/offer';
import {BuyButton} from '../../src/containers/Cart';
import {fetchState} from "../../src/reducers/fetching";
import {getBooksPrice, getCartKeyFromBooks, getCartPrice} from "../../src/entities/cart";

Enzyme.configure({ adapter: new Adapter() });

describe('BuyButton Container', () => {
    let container;
    let state;
    let store;
    let books;
    let offers;
    let cartKey;

    beforeEach(() => {
        books = rawBooks().map(createBook);
        offers = rawOffers().offers.map(createOffer);
        cartKey = getCartKeyFromBooks([books[0], books[1], books[1]]);
        state = Map({
            books: Map({
                [books[0].isbn]: books[0],
                [books[1].isbn]: books[1],
            }),
            cart: Map({
                [books[0].isbn]: 1,
                [books[1].isbn]: 2
            }),
            offers: Map({
                [cartKey]: Set(offers)
            }),
            fetching: Map({
                books: fetchState.SUCCESS,
                offers: Map({
                    [cartKey]: fetchState.SUCCESS
                })
            }),
        });

        store = createMockStore(state);
        container = Enzyme.mount(
            <BuyButton store={store} />
        )
    });

    it('gets the right bookList', () => {
        expect(container.children().props().bookList).toEqual(["c8fabf68-8374-48fe-a7ea-a00ccd07afff", "a460afed-e5e7-4e39-a39d-c885c05db861", "a460afed-e5e7-4e39-a39d-c885c05db861"]);
    });

    it('gets the right cartPrice', () => {
        expect(container.children().props().cartPrice).toBe(getCartPrice(offers, [books[0], books[1], books[1]]));
    });

    it('gets the right offersState', () => {
        expect(container.children().props().offersState).toBe(fetchState.SUCCESS);
    });

    it('gets the right booksPrice', () => {
        expect(container.children().props().booksPrice).toBe(getBooksPrice([books[0], books[1], books[1]]));
    });

    it('gets a cart price equal to 0 if the offers fetchState is different from fetchState.SUCCESS', () => {
        state = state.setIn(['fetching', 'offers', cartKey], fetchState.PENDING);
        store = createMockStore(state);
        const c2 = Enzyme.mount(
            <BuyButton store={store} />
        );
        expect(c2.children().props().cartPrice).toBe(0);
    });

    it('gets an offersState equals to fetchState.PENDING if the offers for the current cart are not in the state', () => {
        state = state.deleteIn(['fetching', 'offers', cartKey]);
        store = createMockStore(state);
        const c2 = Enzyme.mount(
            <BuyButton store={store} />
        );
        expect(c2.children().props().offersState).toBe(fetchState.PENDING);
    });

    describe('actions', () => {
        const offersAction = require("../../src/actions/offers");

        beforeAll(() => {
            offersAction.fetchCommercialOffers = jest.fn();
        });

        it('dispatches the right action when calling the fetchOffers property', () => {
            container.children().props().fetchOffers();
            expect(offersAction.fetchCommercialOffers).toHaveBeenCalled();
        });

        afterAll(() => {
            offersAction.fetchCommercialOffers.mockRestore();
        })
    });
});