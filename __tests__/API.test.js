import {booksRoute, commercialOffersRoute, getBooks, getCommercialOffers} from "../src/API";
import moxios from "moxios";
import {rawBooks} from "./entities/fixtures/books";
import {rawOffers} from "./entities/fixtures/offers";

describe('API', () => {

    beforeEach(() => {
        moxios.install();
        moxios.delay = 1;
    });

    describe('fetchBooks', () => {
        it('returns a list of books on a standard response', () => {
            moxios.stubRequest(booksRoute(), {
                status: 200,
                response: rawBooks()
            });

            return getBooks().then((bookList) => {
                expect(bookList).toEqual(rawBooks());
            });
        });

        it('throws an error on an invalid response format', () => {
            moxios.stubRequest(booksRoute(), {
                status: 200,
                response: {foo: 'bar'}
            });

            return getBooks().catch((e) => {
                expect(e).toEqual(new Error("Invalid response for getBooks() : Expected array, but got something else"));
            });
        });
    });

    describe('offers', () => {
        it('returns a list of offers on a standard response for offers', () => {
            const bookList = rawBooks().map(({isbn}) => isbn);
            moxios.stubRequest(commercialOffersRoute(bookList), {
                status: 200,
                response: rawOffers()
            });

            return getCommercialOffers(bookList).then((offers) => {
                expect(offers).toEqual(rawOffers().offers);
            });
        });

        it('throws an error on an invalid response format', () => {
            const bookList = rawBooks().map(({isbn}) => isbn);
            moxios.stubRequest(commercialOffersRoute(bookList), {
                status: 200,
                response: {foo: 'bar'}
            });

            return getCommercialOffers(bookList).catch((e) => {
                expect(e).toEqual("Invalid response for getOffers() : Expected response.offers to be an array, but got something else");
            });
        });
    });
});