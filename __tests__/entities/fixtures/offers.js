export const rawOffer = () => ({
    "type": "slice",
    "sliceValue": 100,
    "value": 12
});

export const rawOffers = () => ({
    "offers": [
        {
            "type": "percentage",
            "value": 4
        },
        {
            "type": "minus",
            "value": 15
        },
        {
            "type": "slice",
            "sliceValue": 100,
            "value": 12
        }
    ]
});