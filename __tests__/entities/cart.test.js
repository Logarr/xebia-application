import {List} from 'immutable'

import {createCart, getCartKeyFromBooks, getCartPrice} from '../../src/entities/cart'
import {createBook} from "../../src/entities/book";
import {createOffer, MINUS, PERCENTAGE, SLICE} from "../../src/entities/offer";
import {rawBooks} from "./fixtures/books";

describe('cart', () => {
    it('creates a cart', () => {
        const books = rawBooks().map(createBook);

        const cart = createCart(books);

        expect(cart.books).toEqual(List(books.map(({isbn}) => isbn)));
    });

    it('handles cart creation without offers', () => {
        const books = rawBooks().map(createBook);

        const cart = createCart(books);

        expect(cart.books).toEqual(List(books.map(({isbn}) => isbn)));
    });

    it('handles empty cart creation', () => {
        const cart = createCart();

        expect(cart.books).toEqual(List());
    });

    it('computes the best offer', () => {
        const books = Array(5).fill(null).map(() => createBook({price: 50}));
        const offers = [PERCENTAGE, MINUS, SLICE].map((type) => createOffer({type, value: 10, sliceValue: 150}));

        expect(getCartPrice(offers, books)).toBe(225);
    });

    it('gives the best price even if offers are giving a higher price than the raw price', () => {
        const books = Array(5).fill(null).map(() => createBook({price: 50}));
        const offers = [PERCENTAGE, MINUS, SLICE].map((type) => createOffer({type, value: -10, sliceValue: 150}));

        expect(getCartPrice(offers, books)).toBe(250);
    });

    describe('getCartKeyFromBooks', () => {
        it('does not give an empty key for an empty cart', () => {
            expect(getCartKeyFromBooks().length).toBeGreaterThan(0);
        });

        it('gives the same key for the same list of isbns in different order', () => {
            const i1 = {isbn: '1a01as230a'};
            const i2 = {isbn: '1qmnsadfo3'};
            const i3 = {isbn: '1qmnsadfo3'};

            const firstKey = getCartKeyFromBooks([i1, i2, i3]);

            const arrangements = [
                [i1, i3, i2],
                [i2, i1, i3],
                [i2, i3, i1],
                [i3, i1, i2],
                [i3, i2, i1]
            ];

            expect(arrangements.map(getCartKeyFromBooks).every(key => key === firstKey));
        })
    });
});