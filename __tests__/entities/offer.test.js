import {rawOffer} from "./fixtures/offers";

import {createOffer, PERCENTAGE, MINUS, SLICE, computeOfferPrice} from '../../src/entities/offer'
import {createBook} from "../../src/entities/book";
import {getBooksPrice} from "../../src/entities/cart";

describe('offers', () => {
    it('creates an offer', () => {
        const remoteOffer = rawOffer();

        const offer = createOffer(remoteOffer);

        expect(offer.type).toBe(remoteOffer.type);
        expect(offer.value).toBe(remoteOffer.value);
        expect(offer.sliceValue).toBe(remoteOffer.sliceValue);
    });

    describe('offers types', () => {
        let books;
        let type;

        beforeEach(() => {
            books = Array(3).fill(null).map(() => createBook({price: 20}));
        });

        it('throws an error if an unknown type is provided', () => {
            expect(() => createOffer({books, type: 'foobar', value: 10, sliceValue: 10})).toThrow();
        });

        describe('minus', () => {
            beforeAll(() => {
                type = MINUS;
            });

            it('computes a minus type offer', () => {
                const value = 10;

                const offer = createOffer({books, type, value});
                expect(computeOfferPrice(offer)(getBooksPrice(books))).toBe(60 - value);
            });

            it('never computes to a negative price', () => {
                const value = 65;

                const offer = createOffer({books, type, value});
                expect(computeOfferPrice(offer)(getBooksPrice(books))).toBe(0);
            });
        });

        describe('percentage', () => {
            beforeAll(() => {
                type = PERCENTAGE;
            });

            it('computes a percentage type offer', () => {
                const value = 10;

                const offer = createOffer({books, type, value});
                expect(computeOfferPrice(offer)(getBooksPrice(books))).toBe(54);
            });

            it('never computes to a negative price', () => {
                const value = 110;

                const offer = createOffer({books, type, value});
                expect(computeOfferPrice(offer)(getBooksPrice(books))).toBe(0);
            });
        });

        describe('slice', () => {
            beforeAll(() => {
                type = SLICE;
            });

            it('computes a slice type offer', () => {
                const value = 5;
                const sliceValue = 10;

                const offer = createOffer({books, type, value, sliceValue});
                expect(computeOfferPrice(offer)(getBooksPrice(books))).toBe(30);
            });

            it('computes a slice type offer if the amount is below the given slice', () => {
                const value = 10;
                const sliceValue = 200;

                const offer = createOffer({books, type, value, sliceValue});
                expect(computeOfferPrice(offer)(getBooksPrice(books))).toBe(60);
            });

            it('never computes to a negative price', () => {
                const value = 200;
                const sliceValue = 10;

                const offer = createOffer({books, type, value, sliceValue});
                expect(computeOfferPrice(offer)(getBooksPrice(books))).toBe(0);
            });

            it('throws an error if an invalid slice value is provided', () => {
                const value = 200;

                expect(() => createOffer({books, type, value, sliceValue: 0})).toThrow();
                expect(() => createOffer({books, type, value, sliceValue: -10})).toThrow();
                expect(() => createOffer({books, type, value})).toThrow();
            });
        });
    });
});