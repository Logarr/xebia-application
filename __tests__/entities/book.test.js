import {List} from 'immutable'

import {rawBook} from "./fixtures/books";

import {createBook, maxPriceFilter, textFilter, priceSort, titleSort} from '../../src/entities/book'

describe('books', () => {
    it('creates a book', () => {
        const remoteBook  = rawBook();

        const book = createBook(remoteBook);

        expect(book.isbn).toBe(remoteBook.isbn);
        expect(book.title).toBe(remoteBook.title);
        expect(book.price).toBe(remoteBook.price);
        expect(book.cover).toBe(remoteBook.cover);
        expect(book.synopsis).toEqual(List(remoteBook.synopsis));
    });

    describe('sorting functions', () => {
        it('returns a price sorting function respecting the order argument', () => {
            const cheap = {price: 2};
            const average = {price: 5};
            const expensive = {price: 10};

            expect([average, cheap, expensive].sort(priceSort('asc'))).toEqual([cheap, average, expensive]);
            expect([average, cheap, expensive].sort(priceSort('desc'))).toEqual([expensive, average, cheap]);
        });

        it('returns a title sorting function respecting the order argument', () => {
            const a = {title: 'a'};
            const b = {title: 'b'};
            const c = {title: 'c'};

            expect([b, a, c].sort(titleSort('asc'))).toEqual([a, b, c]);
            expect([b, a, c].sort(titleSort('desc'))).toEqual([c, b, a]);
        });

        it('handles accented and cased characters', () => {
            const accented = Array.from("Ça été Mičić. ÀÉÏÓÛ").map((c) => ({title: c}));
            const expected = Array.from("   .ÀačćÇééÉiiÏMÓtÛ").map((c) => ({title: c}));

            expect(accented.sort(titleSort('asc'))).toEqual(expected);
        });
    });

    describe('filtering functions', () => {
        it('returns a price filter function that sorts out every item with a price strictly greater than the passed argument', () => {
            const cheap = {price: 2};
            const average = {price: 5};
            const expensive = {price: 10};

            expect([average, cheap, expensive].filter(maxPriceFilter(10))).toEqual([average, cheap, expensive]);
            expect([average, cheap, expensive].filter(maxPriceFilter(9))).toEqual([average, cheap]);
            expect([average, cheap, expensive].filter(maxPriceFilter(4))).toEqual([cheap]);
            expect([average, cheap, expensive].filter(maxPriceFilter(0))).toEqual([]);
        });

        it('returns a text filtering function that sorts out every item which does not contains the given argument in their title or synopsis', () => {
            const b1 = {title: 'foo', synopsis: ['bar', 'baz', 'qux']};
            const b2 = {title: 'ga', synopsis: ['bu', 'zo', 'meu']};
            const b3 = {title: 'baba', synopsis: ['bubu']};

            expect([b1, b2, b3].filter(textFilter('ba'))).toEqual([b1, b3]);
        });

        it('handles accented and uppercased strings', () => {
            const book = {title: "Café olé", synopsis: ''};

            expect([book].filter(textFilter('cafe'))).toEqual([book]);
        });
    });
});