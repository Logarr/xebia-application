import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import StateHandlerHOC from '../../src/HOC/StateHandlerHOC';
import {fetchState} from "../../src/reducers/fetching";

Enzyme.configure({ adapter: new Adapter() });

describe('State Handler HOC', () => {
    let BaseComponent;
    let LoadingComponent;
    let FailureComponent;
    let PendingComponent;
    let HOC;

    beforeAll(() => {
        BaseComponent = () => 'base';
        LoadingComponent = () => 'loading';
        FailureComponent = () => 'failure';
        PendingComponent = () => 'pending';
        HOC = StateHandlerHOC('fetchState', {LoadingComponent, FailureComponent, PendingComponent})(BaseComponent);
    });

    it('renders the base component if the provided state is not one of PENDING, FAILURE, or FETCHING', () => {
        expect(Enzyme.shallow(<HOC fetchState={'foobar'}/>).html()).toBe('base');
    });

    it('renders the Loading Component if the provided state is FETCHING', () => {
        expect(Enzyme.shallow(<HOC fetchState={fetchState.FETCHING}/>).html()).toBe('loading');
    });

    it('renders the Pending Component if the provided state is PENDING', () => {
        expect(Enzyme.shallow(<HOC fetchState={fetchState.PENDING}/>).html()).toBe('pending');
    });

    it('renders the Loading Component if the provided state is FETCHING', () => {
        expect(Enzyme.shallow(<HOC fetchState={fetchState.FAILURE}/>).html()).toBe('failure');
    });

    it('renders the base component if the needed state component is not provided', () => {
        HOC = StateHandlerHOC('fetchState')(BaseComponent);
        expect(Enzyme.shallow(<HOC fetchState={fetchState.FAILURE}/>).html()).toBe('base');
    });
});