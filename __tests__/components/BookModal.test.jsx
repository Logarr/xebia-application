import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

import BookModal from '../../src/components/BookModal';
import {ModalContainer, ModalContent, Header, CloseButton, TopPanel, Synopsis} from "../../src/styled/BookModal.style";
import {StyledCartActionButton} from "../../src/styled/Buttons.style";

Enzyme.configure({ adapter: new Adapter() });

describe('BookModal', () => {
    let shallowBookModal;
    beforeEach(() => {
        shallowBookModal = Enzyme.shallow(
            <BookModal
                isbn={'isbnsample'}
                title={'Title Sample'}
                price={10}
                cover={'http://sample.url'}
                qty={20}
                synopsis={['foo', 'bar', 'baz', 'qux']}
                addToCart={() => {}}
                removeFromCart={() => {}}
                closeBook={() => {}}
            />
        );
    });

    describe('modal', () => {
        it('renders a Modal container styled component which triggers the closeBook prop function when clicked', () => {
            const spy = sinon.spy();
            shallowBookModal.setProps({closeBook: spy});
            shallowBookModal.find(ModalContainer).simulate('click', new MouseEvent('click'));
            expect(spy.callCount).toBe(1);
        });

        it('renders a Modal content styled component which stops event propagation', () => {
            const spy = sinon.spy();
            shallowBookModal.find(ModalContent).simulate('click', {stopPropagation: spy});
            expect(spy.callCount).toBe(1);
        });
    });

    describe('header', () => {
        it('renders the title component', () => {
            expect(shallowBookModal.find(Header).html()).toContain('Title Sample');
        });

        it('renders a button which triggers the closeBook prop function when clicked', () => {
            const spy = sinon.spy();
            shallowBookModal.setProps({closeBook: spy});
            shallowBookModal.find(CloseButton).simulate('click', new MouseEvent('click'));
            expect(spy.callCount).toBe(1);
        });
    });

    describe('top panel', () => {
       it('renders the price appended with $', () => {
           expect(shallowBookModal.find(TopPanel).html()).toContain('10$');
       });

        it('renders the qty', () => {
            expect(shallowBookModal.find(TopPanel).html()).toContain('20');
        });

        it('renders an img with the right url and an alt attribute', () => {
            expect(shallowBookModal.find(TopPanel).find('img').props().src).toBe('http://sample.url');
            expect(shallowBookModal.find(TopPanel).find('img').props().alt.length).toBeGreaterThan(0);
        });

        it('renders a StyledCartActionButton styled component which triggers the addToCart prop function on click', () => {
            const spy = sinon.spy();
            shallowBookModal.setProps({addToCart: spy});
            shallowBookModal.find(TopPanel).find(StyledCartActionButton).filterWhere((c) => c.text() === '+').simulate('click', new MouseEvent('click'));
            expect(spy.callCount).toBe(1);
        });

        it('renders a StyledCartActionButton styled component which triggers the removeFromCart prop function on click', () => {
            const spy = sinon.spy();
            shallowBookModal.setProps({removeFromCart: spy});
            shallowBookModal.find(TopPanel).find(StyledCartActionButton).filterWhere((c) => c.text() === '-').simulate('click', new MouseEvent('click'));
            expect(spy.callCount).toBe(1);
        });

        it('renders a Synopsis styled component which contains the whole synopsis', () => {
            expect(shallowBookModal.find(Synopsis).text()).toBe('foo bar baz qux');
        });
    });

});