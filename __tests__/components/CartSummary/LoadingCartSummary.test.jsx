import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import LoadingCartSummary from '../../../src/components/CartSummary/LoadingCartSummary';
import EllipsisLoader from '../../../src/components/EllipsisLoader'

Enzyme.configure({ adapter: new Adapter() });

describe('LoadingCartSummary', () => {
    let shallowLoadingCartSummary;
    beforeEach(() => {
        shallowLoadingCartSummary = Enzyme.shallow(
            <LoadingCartSummary/>
        );
    });

    it('renders a Loader', () => {
        expect(shallowLoadingCartSummary.find(EllipsisLoader).length).toBe(1);
    });
});