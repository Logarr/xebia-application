import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

import CartSummary from '../../../src/components/CartSummary/CartSummary';
import {Cart} from "../../../src/styled/App.style";

Enzyme.configure({ adapter: new Adapter() });

describe('CartSummary', () => {
    let shallowCartSummary;
    beforeEach(() => {
        shallowCartSummary = Enzyme.shallow(
            <CartSummary
                cartPrice={10}
                history={{push: () => {}}}
            />
        );
    });

    it('renders a Cart styled component containing the cartPrice prop appended with $', () => {
        expect(shallowCartSummary.find(Cart).text()).toContain('10$');
    });

    it('calls the push method of the history prop when clicked', () => {
        const spy = sinon.spy();
        shallowCartSummary.setProps({history: {push: spy}});
        shallowCartSummary.find(Cart).simulate('click');

        expect(spy.callCount).toBe(1);
        expect(spy.calledWith('/cart')).toBe(true);
    });
});