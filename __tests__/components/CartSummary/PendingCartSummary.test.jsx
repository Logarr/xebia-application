import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

import PendingCartSummary from '../../../src/components/CartSummary/PendingCartSummary';

Enzyme.configure({ adapter: new Adapter() });

describe('PendingCartSummary', () => {
    let shallowPendingCartSummary;
    beforeEach(() => {
        shallowPendingCartSummary = Enzyme.shallow(
            <PendingCartSummary
                bookList={['1','2','3']}
                booksPrice={10}
                fetchOffers={() => {}}
                history={{push: () => {}}}
            />
        );
    });

    it('triggers the history.push method when clicking on the root element', () => {
        const spy = sinon.spy();
        shallowPendingCartSummary.setProps({history: {push: spy}});
        shallowPendingCartSummary.simulate('click');

        expect(spy.callCount).toBe(1);
        expect(spy.calledWith('/cart')).toBe(true);
    });

    it('renders the booksPrice property appended with $', () => {
        expect(shallowPendingCartSummary.text()).toContain('10$');
    });

    it('renders a link that triggers the fetchOffers property on click', () => {
        const spy = sinon.spy();
        shallowPendingCartSummary.setProps({fetchOffers: spy});
        shallowPendingCartSummary.find('a').simulate('click', new MouseEvent('click'));

        expect(spy.callCount).toBe(1);
    });

    it('calls stopPropagation on the link click', () => {
        const spy = sinon.spy();
        shallowPendingCartSummary.find('a').simulate('click', {stopPropagation: spy});

        expect(spy.callCount).toBe(1);
    });
});