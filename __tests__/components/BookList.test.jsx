import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

import BookList from '../../src/components/BookList';
import BookListHeader from '../../src/containers/BookListHeader';

import {BooksContainer} from "../../src/styled/BookList.style";
import {RegularBookCard} from "../../src/containers/Book";

Enzyme.configure({ adapter: new Adapter() });

describe('BookList', () => {
    let shallowBookList;
    beforeEach(() => {
        shallowBookList = Enzyme.shallow(
            <BookList
                fetchBooks={() => {}}
                bookList={['1','2','3']}
            />
        );
    });

    it('renders a book list header', () => {
        expect(shallowBookList.find(BookListHeader).length).toEqual(1);
    });

    it('renders a books container containing a BookCard for each isbn provided', () => {
        expect(shallowBookList.find(BooksContainer).find(RegularBookCard).length).toBe(3);
    });

    it('dispatches the fetchBooks() prop when constructing', () => {
        const spy = sinon.spy();
        Enzyme.shallow(
            <BookList
                fetchBooks={spy}
                bookList={['1','2','3']}
            />
        );
        expect(spy.callCount).toBe(1);
    });
});