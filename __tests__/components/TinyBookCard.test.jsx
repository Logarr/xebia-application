import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

import TinyBookCard from '../../src/components/TinyBookCard'
import {Title, Cover, Qty, Price, Synopsis, TinyBookCardContainer} from "../../src/styled/TinyBookCard.style";
import {StyledCartActionButton} from "../../src/styled/Buttons.style";

Enzyme.configure({ adapter: new Adapter() });

describe('TinyBookCard', () => {
    let shallowTinyBookCard;
    beforeAll(() => {
        shallowTinyBookCard = Enzyme.shallow(
            <TinyBookCard
                isbn={'isbnsample'}
                title={'Title Sample'}
                price={10}
                cover={'http://sample.url'}
                synopsis={['sample', 'synopsis']}
                qty={20}
                addToCart={() => {}}
                removeFromCart={() => {}}
                openBook={() => {}}
            />
        );
    });

    it('calls the openBook property function on the root component click', () => {
        const spy = sinon.spy();
        shallowTinyBookCard.setProps({openBook: spy});
        shallowTinyBookCard.find(TinyBookCardContainer).simulate('click', new MouseEvent('click'));
        expect(spy.callCount).toEqual(1);
        expect(spy.calledWith('isbnsample')).toBe(true);
    });

    it('renders a Title styled component containing the Title', () => {
        expect(shallowTinyBookCard.find(Title).text()).toBe('Title Sample');
    });

    it('renders an img with the right url and an alt', () => {
        expect(shallowTinyBookCard.find(Cover).find('img').props().src).toBe('http://sample.url');
        expect(shallowTinyBookCard.find(Cover).find('img').props().alt.length).toBeGreaterThan(1);
    });

    it('renders a button that triggers the addToCart prop function', () => {
        const spy = sinon.spy();
        shallowTinyBookCard.setProps({addToCart: spy});
        shallowTinyBookCard.find(StyledCartActionButton).filterWhere((c) => c.text() === '+').simulate('click', new MouseEvent('click'));
        expect(spy.callCount).toEqual(1);
        expect(spy.calledWith('isbnsample')).toBe(true);
    });

    it('calls stopPropagation on the event when the addToCart button is clicked', () => {
        const spy = sinon.spy();
        shallowTinyBookCard.find(StyledCartActionButton).filterWhere((c) => c.text() === '+').simulate('click', {stopPropagation: spy});
        expect(spy.callCount).toEqual(1);
    });

    it('renders a button that triggers the removeFromCart prop function', () => {
        const spy = sinon.spy();
        shallowTinyBookCard.setProps({removeFromCart: spy});
        shallowTinyBookCard.find(StyledCartActionButton).filterWhere((c) => c.text() === '-').simulate('click', new MouseEvent('click'));
        expect(spy.callCount).toEqual(1);
        expect(spy.calledWith('isbnsample')).toBe(true);
    });

    it('calls stopPropagation on the event when the addToCart button is clicked', () => {
        const spy = sinon.spy();
        shallowTinyBookCard.find(StyledCartActionButton).filterWhere((c) => c.text() === '-').simulate('click', {stopPropagation: spy});
        expect(spy.callCount).toEqual(1);
    });

    it('displays the quantity in a Qty styled component', () => {
        expect(shallowTinyBookCard.find(Qty).text()).toBe('20');
    });

    it('displays the total price in a Price styled component', () => {
        expect(shallowTinyBookCard.find(Price).text()).toBe('200$');
    });

    it('displays the synopsis in a Synopsis styled component', () => {
        expect(shallowTinyBookCard.find(Synopsis).text()).toBe('sample synopsis');
    });

    it('limits the synopsis length to 600 chars and append an ellipsis "..." if the synopsis is too long', () => {
        shallowTinyBookCard.setProps({synopsis: Array(601).fill('a')});
        expect(shallowTinyBookCard.find(Synopsis).text().length).toBe(603);
        expect(shallowTinyBookCard.find(Synopsis).text().slice(600, 603)).toBe('...');
    });
});