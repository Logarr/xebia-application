import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

import BookListHeader from '../../src/components/BookListHeader';
import {SortContainer, TextSearch} from "../../src/styled/BookList.style";

Enzyme.configure({ adapter: new Adapter() });

describe('BookList header', () => {
    let shallowBookListHeader;
    beforeEach(() => {
        shallowBookListHeader = Enzyme.shallow(
            <BookListHeader
                sort={{key: null, order: 'asc'}}
                filter={{text: '', maxPrice: Infinity}}
                setTextFilter={() => {}}
                setMaxPriceFilter={() => {}}
                toggleTitleSort={() => {}}
                togglePriceSort={() => {}}
            />
        );
    });

    describe('text filter input', () => {
        it('renders an input which renders the text filter value and triggers the setTextFilter prop function when changing', () => {
            const spy = sinon.spy();
            shallowBookListHeader.setProps({setTextFilter: spy, filter: {text: 'foobar', maxPrice: Infinity}});
            shallowBookListHeader.find(TextSearch).simulate('change', {target: {value: 'foo'}});
            expect(shallowBookListHeader.find(TextSearch).props().value).toBe('foobar');
            expect(spy.callCount).toBe(1);
            expect(spy.calledWith('foo')).toBe(true);
        });
    });

    describe('Title sort link', () => {
        it('renders a link that triggers the toggleTitleSort prop function when clicked', () => {
            const spy = sinon.spy();
            shallowBookListHeader.setProps({toggleTitleSort: spy});
            shallowBookListHeader.find(SortContainer).find('a').filterWhere((c) => c.text() === "Title").simulate('click', new MouseEvent('click'));
            expect(spy.callCount).toBe(1);
        });

        it('renders the right className', () => {
            shallowBookListHeader.setProps({sort: {key: 'title', order: 'asc'}});
            expect(shallowBookListHeader.find(SortContainer).find('a').filterWhere((c) => c.text() === "Title").props().className).toBe('asc');
            shallowBookListHeader.setProps({sort: {key: 'title', order: 'desc'}});
            expect(shallowBookListHeader.find(SortContainer).find('a').filterWhere((c) => c.text() === "Title").props().className).toBe('desc');
        });
    });

    describe('Price sort link', () => {
        it('renders a link that triggers the togglePriceSort prop function when clicked', () => {
            const spy = sinon.spy();
            shallowBookListHeader.setProps({togglePriceSort: spy});
            shallowBookListHeader.find(SortContainer).find('a').filterWhere((c) => c.text() === "Price").simulate('click', new MouseEvent('click'));
            expect(spy.callCount).toBe(1);
        });

        it('renders the right className', () => {
            shallowBookListHeader.setProps({sort: {key: 'price', order: 'asc'}});
            expect(shallowBookListHeader.find(SortContainer).find('a').filterWhere((c) => c.text() === "Price").props().className).toBe('asc');
            shallowBookListHeader.setProps({sort: {key: 'price', order: 'desc'}});
            expect(shallowBookListHeader.find(SortContainer).find('a').filterWhere((c) => c.text() === "Price").props().className).toBe('desc');
        });
    });

    describe('max price filter', () => {
        it('renders an input which renders the maxPrice filter value and triggers the setMaxPriceFilter on change', () => {
            const spy = sinon.spy();
            shallowBookListHeader.setProps({setMaxPriceFilter: spy, filter: {text: '', maxPrice: 40}});
            shallowBookListHeader.find(SortContainer).find('input').simulate('change', {target: {value: 'foo'}});
            expect(shallowBookListHeader.find(SortContainer).find('input').props().value).toBe(40);
            expect(spy.callCount).toBe(1);
            expect(spy.calledWith('foo')).toBe(true);
        });

        it('does not render any value if the given max price filter is infinity', () => {
            shallowBookListHeader.setProps({filter: {text: '', maxPrice: Infinity}});
            expect(shallowBookListHeader.find(SortContainer).find('input').props().value).toBe('');
        });
    });
});