import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import BookSelection from '../../src/components/BookSelection';

import {TinyBookCard} from "../../src/containers/Book";

Enzyme.configure({ adapter: new Adapter() });

describe('BookList', () => {
    let shallowBookSelection;
    beforeEach(() => {
        shallowBookSelection = Enzyme.shallow(
            <BookSelection
                bookList={['1','2','3']}
            />
        );
    });

    it('renders a div containing a TinyBookCard for each isbn provided', () => {
        expect(shallowBookSelection.find('div').find(TinyBookCard).length).toBe(3);
    });
});