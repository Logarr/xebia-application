import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import BookCard from '../../src/components/BookCard';
import {Header, Qty, Price, Container} from "../../src/styled/BookCard.style";
import sinon from 'sinon';
import {StyledCartActionButton} from "../../src/styled/Buttons.style";


Enzyme.configure({ adapter: new Adapter() });

describe('BookCard', () => {
    let shallowBookCard;
    beforeEach(() => {
        shallowBookCard = Enzyme.shallow(
            <BookCard
                isbn={'isbnsample'}
                title={'Title Sample'}
                price={10}
                cover={'http://sample.url'}
                qty={20}
                addToCart={() => {}}
                removeFromCart={() => {}}
                openBook={() => {}}
            />
        );
    });

    it('calls the openBook property function on the root component click', () => {
        const spy = sinon.spy();
        shallowBookCard.setProps({openBook: spy});
        shallowBookCard.find(Container).simulate('click', new MouseEvent('click'));
        expect(spy.callCount).toEqual(1);
        expect(spy.calledWith('isbnsample')).toBe(true);
    });

    it('renders a Header styled component containing the title', () => {
        expect(shallowBookCard.find(Header).text()).toBe("Title Sample");
    });

    it('renders a cover image with the right link and a alt', () => {
        const imgProps = shallowBookCard.find('img').props();
        expect(imgProps.src).toBe('http://sample.url');
        expect(imgProps.alt.length).toBeGreaterThan(0);
    });

    it('renders a button that triggers the addToCart prop function', () => {
        const spy = sinon.spy();
        shallowBookCard.setProps({addToCart: spy});
        shallowBookCard.find(StyledCartActionButton).filterWhere((c) => c.text() === '+').simulate('click', new MouseEvent('click'));
        expect(spy.callCount).toEqual(1);
        expect(spy.calledWith('isbnsample')).toBe(true);
    });

    it('calls stopPropagation on the passed event on the addToCart button', () => {
        const spy = sinon.spy();
        shallowBookCard.find(StyledCartActionButton).filterWhere((c) => c.text() === '+').simulate('click', {stopPropagation: spy});
        expect(spy.callCount).toEqual(1);
    });

    it('renders a button that triggers the removeFromCart prop function', () => {
        const spy = sinon.spy();
        shallowBookCard.setProps({removeFromCart: spy});
        shallowBookCard.find(StyledCartActionButton).filterWhere((c) => c.text() === '-').simulate('click', new MouseEvent('click'));
        expect(spy.callCount).toEqual(1);
        expect(spy.calledWith('isbnsample')).toBe(true);
    });

    it('calls stopPropagation on the passed event on the removeFromCart button', () => {
        const spy = sinon.spy();
        shallowBookCard.find(StyledCartActionButton).filterWhere((c) => c.text() === '-').simulate('click', {stopPropagation: spy});
        expect(spy.callCount).toEqual(1);
    });

    it('renders the selected quantity in a Qty styled component', () => {
        expect(shallowBookCard.find(Qty).text()).toBe("20");
    });

    it('does not display the qty prop if it is set to 0', () => {
        shallowBookCard.setProps({qty: 0});
        expect(shallowBookCard.find(Qty).text()).toBe('');
    });

    it('renders a price appended with a $ sign in a Price styled component', () => {
        expect(shallowBookCard.find(Price).text()).toBe('10$');
    });
});