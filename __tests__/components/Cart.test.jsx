import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Cart from '../../src/components/Cart';
import {BuyButton} from '../../src/containers/Cart';
import BooksSelection from '../../src/containers/BooksSelection';

Enzyme.configure({ adapter: new Adapter() });

describe('Cart', () => {
    let shallowCart;
    beforeAll(() => {
        shallowCart = Enzyme.shallow(<Cart />
        );
    });

    it('renders a Buy Button', () => {
        expect(shallowCart.find(BuyButton).length).toBe(1);
    });

    it('renders a Book Selection', () => {
        expect(shallowCart.find(BooksSelection).length).toBe(1);
    });
});