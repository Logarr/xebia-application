import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from '../../src/components/App';
import {Title} from "../../src/styled/App.style";
import {CartSummary} from '../../src/containers/Cart'
import BookList from '../../src/containers/BookList';
import Cart from '../../src/components/Cart';
import {Route, Link} from 'react-router-dom';

Enzyme.configure({ adapter: new Adapter() });

describe('App', () => {
    let shallowApp;
    beforeAll(() => {
        shallowApp = Enzyme.shallow(<App />
        );
    });

    it('renders a Title styled component containing a link to home', () => {
        expect(shallowApp.find(Title).find(Link).props().to).toBe("/");
    });

    it('renders a CartSummary component', () => {
        expect(shallowApp.find(CartSummary).length).toEqual(1);
    });

    it('renders a BookList on the home path via a Route component', () => {
        expect(shallowApp.find(Route).findWhere((c) => c.props().path === '/').props().component).toEqual(BookList);
    });

    it('renders a Cart on the /cart path via a Route component', () => {
        expect(shallowApp.find(Route).findWhere((c) => c.props().path === '/cart').props().component).toEqual(Cart);
    });
});