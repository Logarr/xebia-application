import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import FailureBuyButton from '../../../src/components/BuyButton/FailureBuyButton';
import {DiscountPrice} from "../../../src/styled/BuyButton.style";

Enzyme.configure({ adapter: new Adapter() });

describe('FailureBuyButton', () => {
    let shallowFailureBuyButton;
    beforeEach(() => {
        shallowFailureBuyButton = Enzyme.shallow(
            <FailureBuyButton
                bookList={['1','2','3']}
                booksPrice={20}
            />
        );
    });

    it('renders a Discount Price styled component which shows the bookPrice property append by $', () => {
        expect(shallowFailureBuyButton.find(DiscountPrice).text()).toBe('20$');
    });

    it('does not render anything if there is no books', () => {
        shallowFailureBuyButton.setProps({bookList: []});
        expect(shallowFailureBuyButton.text().length).toBe(0);
    });
});