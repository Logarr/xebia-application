import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import BuyButton from '../../../src/components/BuyButton/BuyButton';
import {FullPrice, DiscountPrice} from "../../../src/styled/BuyButton.style";

Enzyme.configure({ adapter: new Adapter() });

describe('BuyButton', () => {
    let shallowBuyButton;
    beforeEach(() => {
        shallowBuyButton = Enzyme.shallow(
            <BuyButton
                bookList={['1','2','3']}
                cartPrice={10}
                booksPrice={20}
            />
        );
    });

    it('renders a Full Price styled component which shows the bookPrice property append by $', () => {
        expect(shallowBuyButton.find(FullPrice).text()).toBe('20$');
    });

    it('renders a Discount Price styled component which shows the cartPrice property append by $', () => {
        expect(shallowBuyButton.find(DiscountPrice).text()).toBe('10$');
    });

    it('does not render anything if there is no books', () => {
        shallowBuyButton.setProps({bookList: []});
        expect(shallowBuyButton.text().length).toBe(0);
    });
});