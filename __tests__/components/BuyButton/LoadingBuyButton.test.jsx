import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

import LoadingBuyButton from '../../../src/components/BuyButton/LoadingBuyButton';
import EllipsisLoader from '../../../src/components/EllipsisLoader'
import {fetchState} from "../../../src/reducers/fetching";

Enzyme.configure({ adapter: new Adapter() });

describe('LoadingBuyButton', () => {
    let shallowLoadingBuyButton;
    beforeEach(() => {
        shallowLoadingBuyButton = Enzyme.shallow(
            <LoadingBuyButton
                bookList={['1','2','3']}
                offersState={fetchState.SUCCESS}
                fetchOffers={() => {}}
            />
        );
    });

    it('renders a Loader', () => {
        expect(shallowLoadingBuyButton.find(EllipsisLoader).length).toBe(1);
    });

    it('calls the fetchOffers prop function if the offersState is fetchState.PENDING', () => {
        const spy = sinon.spy();
        Enzyme.shallow(
            <LoadingBuyButton
                bookList={['1','2','3']}
                offersState={fetchState.PENDING}
                fetchOffers={spy}
            />
        );
        expect(spy.callCount).toBe(1);
        expect(spy.calledWith(['1','2','3'])).toBe(true);
    });

    it('does not call the fetchOffers property if the fetchState is anything else than PENDING', () => {
        const spy = sinon.spy();
        Enzyme.shallow(
            <LoadingBuyButton
                bookList={['1','2','3']}
                offersState={fetchState.SUCCESS}
                fetchOffers={spy}
            />
        );
        Enzyme.shallow(
            <LoadingBuyButton
                bookList={['1','2','3']}
                offersState={fetchState.FETCHING}
                fetchOffers={spy}
            />
        );
        Enzyme.shallow(
            <LoadingBuyButton
                bookList={['1','2','3']}
                offersState={fetchState.SUCCESS}
                fetchOffers={spy}
            />
        );
        Enzyme.shallow(
            <LoadingBuyButton
                bookList={['1','2','3']}
                offersState={fetchState.FAILURE}
                fetchOffers={spy}
            />
        );
        expect(spy.callCount).toBe(0);
    });
});